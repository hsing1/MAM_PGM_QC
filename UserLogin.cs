﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MAM_PGM_QC.WSUserObject;
using MAM_PGM_QC.WSASPNetSession;
using MAM_PGM_QC.App_Code;
using MAM_PGM_QC.DataStruct;

namespace MAM_PGM_QC
{
    /// <summary>
    /// 登入畫面
    /// </summary>
    public partial class UserLogin : Form
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();
        logFunction logFun = new logFunction();

        #endregion

        #region - 初始化 -

        public UserLogin()
        {
            InitializeComponent();
        }

        private void UserLogin_Load(object sender, EventArgs e)
        {
            //取得版本資訊
            lblAppVersion.Text = String.Format("軟體版本：{0}", GlobalParameters.AppVersion);
        }

        #endregion

        #region  - 方法 -

        /// <summary>
        /// 帳號、密碼驗證
        /// </summary>
        /// <returns>驗證結果(true、false)</returns>
        private void IsUserVerify()
        {
            WSUserObjectSoapClient userObj = new WSUserObjectSoapClient();

            userObj.loginAsync(_UID, _PWD);

            userObj.loginCompleted += (s, args) =>
            {
                if (args.Error == null)
                {
                    if (args.Result != null)
                    {
                        if (args.Result == string.Empty)
                        {
                            UserBo userBo = new UserBo();
                            userBo.vo = userBo.GetUserData(_UID);

                            GlobalUserData.UID = userBo.vo.UID;
                            GlobalUserData.CName = userBo.vo.CName;
                            GlobalUserData.ComputerName = Environment.MachineName;
                            GlobalUserData.IsActive = userBo.vo.IsActive;

                            logFun.LogInfo("MAM_PGM_QC.UserLogin.IsUserVerify()", String.Format("使用者：{0}({1})、電腦：{2}、軟體版本：{3}", GlobalUserData.CName, GlobalUserData.UID, GlobalUserData.ComputerName, GlobalParameters.AppVersion));

                            SystemConfigBo configBo = new SystemConfigBo();
                            configBo.ReadSystemConfig();
                            GlobalParameters.HdUploadFilePath = configBo.vo.PTS_AP_UPLOAD_PATH;
                            GlobalParameters.WaitQCFilePath = configBo.vo.PendingPath;
                            GlobalParameters.DoneQCFilePath = configBo.vo.CompletePath;

                            //取得權限模組
                            ModuleClass.getModulePermissionByUserId(GlobalUserData.UID);
                            bool IsHasQC = ModuleClass.getModulePermission();

                            //是否有QC權限
                            if (IsHasQC)
                            {
                                this.Hide();

                                RadMainForm frmMain = new RadMainForm();
                                frmMain.ShowDialog();

                                if (frmMain.DialogResult == DialogResult.Cancel)
                                {
                                    Application.Exit();
                                }
                            }
                            else
                            {
                                logFun.LogInfo("MAM_PGM_QC.UserLogin.IsUserVerify()", String.Format("使用者：{0}({1})、軟體版本：{2}，無QC使用權限！", GlobalUserData.CName, GlobalUserData.UID, GlobalParameters.AppVersion));
                                MessageBox.Show("帳號：" + _UID + "，無QC使用權限！");
                                Application.Restart();
                            }
                        }
                        else
                        {
                            MessageBox.Show("錯誤：" + args.Result);
                            Application.Restart();
                        }
                    }
                }
            };
        }

        #endregion

        #region - 事件 -

        #region Button

        /// <summary>
        /// 登入
        /// </summary>
        private void btnSubmit_Click(object sender, EventArgs e)
        {
            _UID = txtUserID.Text;
            _PWD = txtUserPW.Text;

            if (!String.IsNullOrEmpty(_UID) && !String.IsNullOrEmpty(_PWD))
            {
                //防止兩次
                this.btnSubmit.Click -= new EventHandler(btnSubmit_Click);
                this.DialogResult = DialogResult.OK;

                IsUserVerify();
            }
            else 
            {
                MessageBox.Show("帳號或密碼不可以空白！");
            }
        }

        /// <summary>
        /// 取消
        /// </summary>
        private void btnCancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        #endregion

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// 使用者帳號
        /// </summary>
        private string _UID;

        /// <summary>
        /// 使用者密碼
        /// </summary>
        private string _PWD;

        #endregion
    }
}