﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MAM_PGM_QC
{
    static class Program
    {
        /// <summary>
        /// 應用程式的主要進入點。
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                MPlatformSDKLic.IntializeProtection();
                EncoderlibLic.IntializeProtection();
                DecoderlibLic.IntializeProtection();

                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);

                #region - 防止程式雙開 -

                bool v_mutexflag = false;

                Mutex mutex = new Mutex(true, "MAM_PGM_QC", out v_mutexflag);

                if (!v_mutexflag)
                {
                    MessageBox.Show("程式已執行中，無法再一次執行...");
                    Thread.Sleep(1000);
                    Environment.Exit(1);
                }

                #endregion

                Application.Run(new UserLogin());
            }
            catch (Exception)
            {

            }
        }
    }
}