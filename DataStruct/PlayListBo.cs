﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using MAM_PGM_QC.App_Code;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// 播表 PlayList 資料表 操作
    /// </summary>
    class PlayListBo
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();
        logFunction logFun = new logFunction();
        public PlayListVo vo = new PlayListVo();

        #endregion

        #region - 查詢 -

        /// <summary>
        /// 播表資料查詢
        /// </summary>
        public DataTable GetPlayList()
        {
            try
            {
                strSQL = "";
                strSQL += "EXEC [SP_Q_GET_PLAYLIST_CHECK_PENDING] '" + vo.FDDATE + "','" + vo.FSCHANNEL_ID + "','playlist' ";

                DataTable dt = dbFun.CreateDataTable(strSQL);
                DataTable dtPlus = new DataTable();
                //DataTable dtPlus = dt.Clone();

                if (dt.Rows.Count != 0)
                {
                    Hashtable htPROPERTY = GlobalMsgTxt.CodeToMsg("PROPERTY");
                    Hashtable htLIVE = GlobalMsgTxt.CodeToMsg("LIVE");
                    Hashtable htVIDEO = GlobalMsgTxt.CodeToMsg("VIDEO");
                    Hashtable htVIDEO_HDMC = GlobalMsgTxt.CodeToMsg("VIDEO_HDMC");

                    dtPlus.Columns.Add("FNCONBINE_NO", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSPLAY_TIME", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSPROG_ID", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSPROG_NAME", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FNEPISODE", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FNBREAK_NO", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FCPROPERTY", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSBEG_TIMECODE", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSEND_TIMECODE", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSDUR", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSARC_TYPE", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSFILE_NO", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSVIDEO_ID", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FSMEMO", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FNLIVE", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FCFILE_STATUS", Type.GetType("System.String"));
                    dtPlus.Columns.Add("FCSTATUS", Type.GetType("System.String"));

                    foreach (DataRow dr in dt.Rows)
                    {
                        DataRow drPlus = dtPlus.NewRow();

                        drPlus["FNCONBINE_NO"] = dr["FNCONBINE_NO"].ToString();
                        drPlus["FSPLAY_TIME"] = dr["FSPLAY_TIME"].ToString();
                        drPlus["FSPROG_ID"] = dr["FSPROG_ID"].ToString();
                        drPlus["FSPROG_NAME"] = dr["FSPROG_NAME"].ToString();
                        drPlus["FNEPISODE"] = dr["FNEPISODE"].ToString();
                        drPlus["FNBREAK_NO"] = dr["FNBREAK_NO"].ToString();
                        drPlus["FCPROPERTY"] = htPROPERTY[dr["FCPROPERTY"].ToString()];
                        drPlus["FSBEG_TIMECODE"] = dr["FSBEG_TIMECODE"].ToString();
                        drPlus["FSEND_TIMECODE"] = dr["FSEND_TIMECODE"].ToString();
                        drPlus["FSDUR"] = dr["FSDUR"].ToString();
                        drPlus["FSARC_TYPE"] = dr["FSARC_TYPE"].ToString();
                        drPlus["FSFILE_NO"] = dr["FSFILE_NO"].ToString();
                        drPlus["FSVIDEO_ID"] = dr["FSVIDEO_ID"].ToString();
                        drPlus["FSMEMO"] = dr["FSMEMO"].ToString();
                        drPlus["FNLIVE"] = htLIVE[dr["FNLIVE"].ToString()];
                        drPlus["FCFILE_STATUS"] = htVIDEO[dr["FCFILE_STATUS"].ToString()];
                        drPlus["FCSTATUS"] = htVIDEO_HDMC[dr["FCSTATUS"].ToString()];

                        dtPlus.Rows.Add(drPlus);
                    }
                }

                return dtPlus;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// 播表資料查詢
        /// </summary>
        public PlayListVo GetPlayList(string VideoID)
        {
            strSQL = "";
            strSQL += "EXEC [SP_Q_GET_PLAYLIST_CHECK_PENDING] '" + vo.FDDATE + "','" + vo.FSCHANNEL_ID + "','playlist' ";

            DataTable dt = dbFun.CreateDataTable(strSQL);

            var distinctRows = 
            (
                from DataRow dr in dt.Rows
                where dr.Field<string>("FSVIDEO_ID") == VideoID
                select new 
                { 
                    Property = dr["FCPROPERTY"],
                    ProgID = dr["FSPROG_ID"],
                    ProgName = dr["FSPROG_NAME"],
                    Episode = dr["FNEPISODE"],
                    FileNo = dr["FSFILE_NO"],
                    Memo = dr["FSMEMO"]
                }
            ).Distinct();

            foreach (var od in distinctRows) 
            {
                vo.FSVIDEO_ID = VideoID;
                vo.FCPROPERTY = od.Property.ToString();
                vo.FSPROG_ID = od.ProgID.ToString();
                vo.FSPROG_NAME = od.ProgName.ToString();
                vo.FNEPISODE = (int)od.Episode;
                vo.FSFILE_NO = od.FileNo.ToString();
                vo.FSMEMO = od.Memo.ToString();
            }

            return vo;
        }

        /// <summary>
        /// 播表資料查詢(QC列表)
        /// </summary>
        public DataTable GetPlayListForQC()
        {
            strSQL = "";
            strSQL += "EXEC [SP_Q_GET_PLAYLIST_CHECK_PENDING] '" + vo.FDDATE + "','" + vo.FSCHANNEL_ID + "','qc' ";

            DataTable dt = dbFun.CreateDataTable(strSQL);

            return dt;
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL = String.Empty;

        #endregion
    }
}