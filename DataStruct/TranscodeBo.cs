﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MAM_PGM_QC.App_Code;
using MAM_PGM_QC.WSSTT;
using MAM_PGM_QC.WSBroadcast;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// 審核通過，觸發轉低解檔 操作
    /// </summary>
    class TranscodeBo
    {
        #region - 宣告 -

        public PlayListVo vo = new PlayListVo();

        WSSTTSoapClient WSSTT_Client = new WSSTTSoapClient();
        WSBROADCASTSoapClient Brocast_Client = new WSBROADCASTSoapClient();

        logFunction logFun = new logFunction();

        #endregion

        #region - 事件 -

        /// <summary>
        /// 審核通過，通知轉低解檔(MP4)
        /// </summary>
        public void SetMP4Transcode(PlayListVo voMp4)
        {
            vo = voMp4;

            string Memo = String.Empty;

            if (!String.IsNullOrEmpty(vo.FSFILE_NO))
            {
                if (vo.FSFILE_NO.Substring(0, 1).Equals("G"))
                {
                    Memo = "{節目名稱：" + vo.FSPROG_NAME + " ，節目編號：" + vo.FSPROG_ID + " ，集別：" + vo.FNEPISODE + " ，檔案類型：" + vo.FSARC_TYPE + "送播(QC)，檔案編號：" + vo.FSFILE_NO + "}";
                }
                else if (vo.FSFILE_NO.Substring(0, 1).Equals("P"))
                {
                    Memo = "{短帶名稱：" + vo.FSPROG_NAME + " ，短帶編號：" + vo.FSPROG_ID + " ，檔案類型：" + vo.FSARC_TYPE + "送播(QC)，檔案編號：" + vo.FSFILE_NO + "}";
                }
            }
        
            StringBuilder strLog = new StringBuilder();
            strLog.Append("影片編號：" + vo.FSVIDEO_ID);
            strLog.Append("、TC_IN：" + vo.FSBEG_TIMECODE_MIN);
            strLog.Append("、TC_OUT：" + vo.FSEND_TIMECODE_MAX);
            strLog.Append("、高解路徑：" + vo.FSFILE_PATH_H);
            strLog.Append("、低解路徑：" + vo.FSFILE_PATH_L);
            strLog.Append("、檔案狀態：" + vo.FCFILE_STATUS);
            strLog.Append("、檔案狀態(HD)：" + vo.FCSTATUS);
            strLog.Append("、解析度：" + vo.FSARC_TYPE);
            strLog.Append("、檢查人：" + GlobalUserData.CName + "(" + GlobalUserData.UID + ")");
            strLog.Append("、檢查日期：" + DateTime.Now.ToString());
            strLog.Append("、電腦：" + GlobalUserData.ComputerName);
            strLog.Append("、軟體版本：" + GlobalParameters.AppVersion);

            logFun.LogTrace("MAM_PGM_QC.TranscodeBo.SetMP4Transcode()", "【" + vo.FSARC_TYPE + "轉低解檔】" + strLog);
            logFun.LogTrace("MAM_PGM_QC.TranscodeBo.SetMP4Transcode()", "【" + vo.FSARC_TYPE + "轉檔備註】" + Memo);

            if (vo.FSARC_TYPE.Equals("HD"))
            {
                WSSTT_Client.CallIngest_HDCompleted += WSSTT_Client_CallIngest_HDCompleted;  //HD註冊事件
                WSSTT_Client.CallIngest_HDAsync(
                    vo.FSFILE_NO,
                    "FILE",
                    vo.FSFILE_PATH_H,
                    vo.FSBEG_TIMECODE_MIN,
                    vo.FSEND_TIMECODE_MAX,
                    vo.FSFILE_PATH_L,
                    Memo,
                    "HD"
                );
            }
            else 
            {
                WSSTT_Client.CallIngest_SDCompleted += WSSTT_Client_CallIngest_SDCompleted; //SD註冊事件
                WSSTT_Client.CallIngest_SDAsync(
                    vo.FSFILE_NO,
                    "FILE",
                    vo.FSFILE_PATH_H,
                    vo.FSBEG_TIMECODE_MIN,
                    vo.FSEND_TIMECODE_MAX,
                    vo.FSFILE_PATH_L,
                    Memo,
                    "SD"
                 );
            }
        }

        /// <summary>
        /// (啟動成功)回傳 Job ID
        /// </summary>
        void WSSTT_Client_CallIngest_HDCompleted(object sender, CallIngest_HDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.StartsWith("E"))
                    {
                        logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_HDCompleted()", "啟動轉檔失敗(更新轉檔的JobID)，原因：" + e.Result.Substring(2));
                    }
                    else
                    {
                        logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_HDCompleted()", String.Format("啟動成功(回傳 Job ID)參數、檔案編號：{0}、結果：{1}、來源檔案名稱：{2}、使用者：{3}", vo.FSFILE_NO, e.Result, vo.FSVIDEO_ID, GlobalUserData.UID));

                        //更新轉檔的JobID        
                        Brocast_Client.UPDATE_TBLOG_VIDEO_JobIDCompleted += Brocast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted;  //註冊事件
                        Brocast_Client.UPDATE_TBLOG_VIDEO_JobIDAsync(
                            vo.FSFILE_NO,
                            e.Result,
                            "來源檔案名稱：" + vo.FSVIDEO_ID,
                            GlobalUserData.UID
                        );
                    }
                }
            }
            else
            {
                logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_HDCompleted()", "啟動轉檔失敗，原因：" + e.Error);
            }
        }

        /// <summary>
        /// (啟動成功)拿 Job ID 去更新 TBLOG_VIDEO
        /// </summary>
        void Brocast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted(object sender, UPDATE_TBLOG_VIDEO_JobIDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                var q = e.Result;
                logFun.LogTrace("MAM_PGM_QC.TranscodeBo.Brocast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted()", "啟動轉檔成功(更新TBLOG_VIDEO)，狀態：" + e.Result);
            }
            else
            {
                logFun.LogTrace("MAM_PGM_QC.TranscodeBo.Brocast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted()", "啟動轉檔失敗(更新TBLOG_VIDEO)，原因：" + e.Error);
            }
        }

        /// <summary>
        /// (啟動成功)回傳 Job ID
        /// </summary>
        void WSSTT_Client_CallIngest_SDCompleted(object sender, CallIngest_SDCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                if (e.Result != null && e.Result != "")
                {
                    if (e.Result.StartsWith("E"))
                    {
                        logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_SDCompleted", "啟動轉檔失敗(更新轉檔的JobID)，原因：" + e.Result.Substring(2));
                    }
                    else
                    {
                        logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_SDCompleted", String.Format("啟動成功(回傳 Job ID)參數、檔案編號：{0}、結果：{1}、來源檔案名稱：{2}、使用者：{3}", vo.FSFILE_NO, e.Result, vo.FSVIDEO_ID, GlobalUserData.UID));

                        //更新轉檔的JobID        
                        Brocast_Client.UPDATE_TBLOG_VIDEO_JobIDCompleted += Brocast_Client_UPDATE_TBLOG_VIDEO_JobIDCompleted;  //註冊事件
                        Brocast_Client.UPDATE_TBLOG_VIDEO_JobIDAsync(
                            vo.FSFILE_NO,
                            e.Result,
                            "來源檔案名稱：" + vo.FSVIDEO_ID,
                            GlobalUserData.UID
                        );
                    }
                }
            }
            else
            {
                logFun.LogTrace("MAM_PGM_QC.TranscodeBo.WSSTT_Client_CallIngest_SDCompleted()", "啟動轉檔失敗，原因：" + e.Error);
            }
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL = String.Empty;

        /// <summary>
        /// 功能或使用者
        /// </summary>
        private string _Action;
        public string Action
        {
            get { return _Action; }
            set { _Action = value; }
        }

        #endregion
    }
}