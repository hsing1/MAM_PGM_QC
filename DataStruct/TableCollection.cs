﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MAM_PGM_QC.DataStruct
{
    #region - 播表 資料表 -

    /// <summary>
    /// 播表 資料表
    /// </summary>
    public class PlayListVo
    {
        /// <summary>序號</summary>
        public string FNPLAYLIST_NO;
        /// <summary>播出日期</summary>
        public string FDDATE;
        /// <summary>頻道代碼</summary>
        public string FSCHANNEL_ID;
        /// <summary>播出時間</summary>
        public string FSPLAY_TIME;
        /// <summary>節目 / 短帶</summary>
        public string FCPROPERTY;
        /// <summary>影片編號</summary>
        public string FSVIDEO_ID;
        /// <summary>節目編號</summary>
        public string FSPROG_ID;
        /// <summary>節目名稱</summary>
        public string FSPROG_NAME;
        /// <summary>集數</summary>
        public int FNEPISODE;
        /// <summary>支數</summary>
        public int FNBREAK_NO;
        /// <summary>IN 點</summary>
        public string FSBEG_TIMECODE;
        /// <summary>OUT 點</summary>
        public string FSEND_TIMECODE;
        /// <summary>最小 IN 點</summary>
        public string FSBEG_TIMECODE_MIN;
        /// <summary>最大 OUT 點</summary>
        public string FSEND_TIMECODE_MAX;
        /// <summary>秒數</summary>
        public int FSDUR;
        /// <summary>解析度</summary>
        public string FSARC_TYPE;
        /// <summary>LIVE</summary>
        public int FNLIVE;
        /// <summary>播表備註</summary>
        public string FSMEMO;

        /// <summary>檔案編號</summary>
        public string FSFILE_NO;
        /// <summary>檔案路徑</summary>
        public string FSFILE_PATH;
        /// <summary>高解檔路徑</summary>
        public string FSFILE_PATH_H;
        /// <summary>低解檔路徑</summary>
        public string FSFILE_PATH_L;
        /// <summary>檔案狀態</summary>
        public string FCFILE_STATUS;
        /// <summary>HD播出檔案狀態</summary>
        public string FCSTATUS;

        /// <summary>QC備註</summary>
        public string FSQC_DESCRIPTION;
        /// <summary>建檔人</summary>
        public string FSCREATED_BY;
        /// <summary>建檔日期</summary>
        public string FDCREATED_DATE;
    }

    #endregion

    #region - 使用者 -

    /// <summary>
    /// 使用者資料表
    /// </summary>
    public class UserVo
    {
        /// <summary>帳號</summary>
        public string UID;
        /// <summary>帳號名稱</summary>
        public string CName;
        /// <summary>是否啟用</summary>
        public string IsActive;
    }

    #endregion

    #region - 系統設定 -

    /// <summary>
    /// 系統設定檔
    /// </summary>
    public class SystemConfigVo
    {
        /// <summary>高解存放區</summary>
        public string PTS_AP_UPLOAD_PATH;
        /// <summary>待審區</summary>
        public string PendingPath;
        /// <summary>審核通過區</summary>
        public string CompletePath;
    }

    #endregion
}