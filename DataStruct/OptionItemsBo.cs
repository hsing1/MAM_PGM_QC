﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// 項目資料表
    /// </summary>
    class OptionItemsBo
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();

        #endregion

        #region - 查詢 -

        /// <summary>
        /// 資料查詢 QC頻道
        /// </summary>
        public DataTable SelectChannelItem()
        {
            strSQL = "";
            strSQL += "SELECT [FSCHANNEL_ID] AS [VALUE],[FSCHANNEL_NAME] AS [TEXT] ";
            strSQL += "FROM [TBZCHANNEL] ";
            strSQL += "WHERE[FBISENABLE]='1' AND [FSCHANNEL_TYPE]='002' ";
            strSQL += "ORDER BY [FSSORT] ";

            DataTable dt = dbFun.DataTableToDropDownList(strSQL);

            return dt;
        }

        /// <summary>
        /// 資料查詢 QC結果
        /// </summary>
        public DataTable SelectQCResultItem()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("TEXT", Type.GetType("System.String"));
            dt.Columns.Add("VALUE", Type.GetType("System.String"));
            dt.Rows.Add(" - 請選擇 - ", "");
            dt.Rows.Add("通過", "O");
            dt.Rows.Add("不通過", "X"); 

            return dt;
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL = String.Empty;

        #endregion
    }
}