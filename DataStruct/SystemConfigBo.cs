﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using MAM_PGM_QC.App_Code;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// MAM 系統設定檔 共用參數
    /// </summary>
    class SystemConfigBo
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();
        logFunction logFun = new logFunction();
        public SystemConfigVo vo = new SystemConfigVo();

        #endregion

        #region - 查詢 -

        /// <summary>
        /// 讀取 MAM 共用設定檔
        /// </summary>
        public SystemConfigVo ReadSystemConfig()
        {
            try
            {
                strSQL = "";
                strSQL += "SELECT [FSSYSTEM_CONFIG] ";
                strSQL += "FROM [TBSYSTEM_CONFIG]";

                DataTable dt = dbFun.CreateDataTable(strSQL);

                string FSSYSTEM_CONFIG = dt.Rows[0]["FSSYSTEM_CONFIG"].ToString();

                if (!string.IsNullOrEmpty(FSSYSTEM_CONFIG))
                {
                    XDocument xdoc = XDocument.Parse(FSSYSTEM_CONFIG);

                    var paths = from config in xdoc.Descendants("PGM_Config")
                                    select new
                                    {
                                         PTS_AP_UPLOAD_PATH = config.Element("PTS_AP_UPLOAD_PATH").Value,
                                         PendingPath = config.Element("PendingPath").Value,
                                         CompletePath = config.Element("CompletePath").Value
                                    };

                    foreach (var path in paths)
                    {
                        vo.PTS_AP_UPLOAD_PATH = path.PTS_AP_UPLOAD_PATH;
                        vo.PendingPath = path.PendingPath;
                        vo.CompletePath = path.CompletePath;
                    }
                }
            }
            catch (Exception ex)
            {
                logFun.LogTrace("MAM_PGM_QC.SystemConfigBo.ReadSystemConfig()", String.Format("讀取 MAM 共用設定檔失敗，原因：{0}、使用者：{1}", ex.ToString(), GlobalUserData.UID));
            }

            return vo;
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL = String.Empty;

        #endregion
    }
}