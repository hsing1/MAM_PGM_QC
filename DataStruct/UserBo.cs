﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MAM_PGM_QC.App_Code;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// 使用者 資料表
    /// </summary>
    class UserBo
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();
        public UserVo vo = new UserVo();

        #endregion

        #region - 查詢 -

        /// <summary>
        /// 資料查詢 (單筆)
        /// </summary>
        /// <param name="UID">帳號</param>
        public UserVo GetUserData(string UID)
        {
            strSQL = "";
            strSQL += "SELECT [FSUSER_ID],[FSUSER_ChtName],[FCACTIVE] ";
            strSQL += "FROM [TBUSERS] ";
            strSQL += "WHERE [FSUSER]='" + UID + "' ";

            DataTable dt = dbFun.CreateDataTable(strSQL);

            UserVo vo = new UserVo();

            if (dt.Rows.Count != 0)
            {
                vo.UID = dt.Rows[0]["FSUSER_ID"].ToString();
                vo.CName = dt.Rows[0]["FSUSER_ChtName"].ToString();
                vo.IsActive = dt.Rows[0]["FCACTIVE"].ToString();
            }

            return vo;
        }

        /// <summary>
        /// 員工編號 和 姓名
        /// </summary>
        public DataTable GetCNameAndUID()
        {
            strSQL = "";
            strSQL += "SELECT [FSUSER_ID],[FSUSER_ChtName] ";
            strSQL += "FROM [TBUSERS] ";
            strSQL += "ORDER BY [FSUSER_ID] ";

            DataTable dt = dbFun.CreateDataTable(strSQL);

            return dt;
        }

        /// <summary>
        /// 帳號是否啟用
        /// </summary>
        /// <param name="UID">帳號</param>
        public bool IsActive(string UID)
        {
            bool vResult = false;

            try
            {
                strSQL = "";
                strSQL += "SELECT [FSUSER_ID],[FSUSER_ChtName],[FCACTIVE] ";
                strSQL += "FROM [TBUSERS] ";
                strSQL += "WHERE 1=1 ";
                strSQL += "          AND [FCACTIVE]='1' ";
                strSQL += "          AND [FSUSER_ID]='" + UID + "' ";

                DataTable dt = dbFun.CreateDataTable(strSQL);

                if (dt.Rows.Count != 0)
                {
                    vResult = true;
                }
            }
            finally
            {
            }

            return vResult;
        }

        /// <summary>
        /// 是否有QC權限
        /// </summary>
        /// <param name="UID">帳號</param>
        public bool IsHasQC(string UID) 
        {
            bool vResult = false;

            try 
	        {
                strSQL = "";
                strSQL += "SELECT [FSUSER_ID] ";
                strSQL += "FROM [TBUSER_GROUP] ";
                strSQL += "WHERE 1=1 ";
                strSQL += "           AND [FSGROUP_ID] IN (SELECT [FSGROUP_ID] FROM [TBMODULE_GROUP] WHERE [FSMODULE_ID]='Q100001') ";
                strSQL += "	          AND [FSUSER_ID]='" + UID + "' ";

                DataTable dt = dbFun.CreateDataTable(strSQL);

                if (dt.Rows.Count != 0)
                {
                    vResult = true;
                }
	        }
            finally
            {
            }

            return vResult;
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL;

        #endregion
    }
}