﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MAM_PGM_QC.App_Code;

namespace MAM_PGM_QC.DataStruct
{
    /// <summary>
    /// QC結果 資料表 操作
    /// </summary>
    class QCResultBo
    {
        #region - 宣告 -

        dbFunction dbFun = new dbFunction();
        logFunction logFun = new logFunction();
        public PlayListVo vo = new PlayListVo();

        #endregion

        #region - 查詢 -

        /// <summary>
        /// 影片QC紀錄查詢
        /// </summary>
        public DataTable GetQCResultList()
        {
            strSQL = "";
            strSQL += "SELECT [ID],[FSVIDEO_ID],[FSFILE_NO],[FSPROG_ID],[FSPROG_NAME],[FNEPISODE],[FSQC_DESCRIPTION],[FSCREATED_BY],[FDCREATED_DATE] ";
            strSQL += "FROM [TBLOG_QC_RESULT] ";
            strSQL += "WHERE [FSVIDEO_ID]='"+ vo.FSVIDEO_ID +"' ";
            strSQL += "ORDER BY [FDCREATED_DATE] DESC";

            DataTable dt = dbFun.CreateDataTable(strSQL);

            return dt;
        }

        /// <summary>
        /// 取得 QC檔案 最小和最大 的TIMECODE(轉低解用)
        /// </summary>
        public bool GetQCFileMinMaxTimeCode()
        {
            bool vResult = false;

            try
            {
                strSQL = "";
                strSQL += "SELECT MIN([FSBEG_TIMECODE]) AS [FSBEG_TIMECODE_MIN], MAX([FSEND_TIMECODE]) AS [FSBEG_TIMECODE_MAX] ";
                strSQL += "FROM [TBLOG_VIDEO_SEG] ";
                strSQL += "WHERE [FSFILE_NO]='" + vo.FSFILE_NO + "' ";

                DataTable dt = dbFun.CreateDataTable(strSQL);

                foreach (DataRow dr in dt.Rows)
                {
                    vo.FSBEG_TIMECODE_MIN = dr["FSBEG_TIMECODE_MIN"].ToString();
                    vo.FSEND_TIMECODE_MAX = dr["FSBEG_TIMECODE_MAX"].ToString();
                }

                vResult = true;
            }
            catch (Exception ex)
            {
                logFun.LogError("MAM_PGM_QC.QCResultBo.GetQCFileMinMaxTimeCode()", ex.ToString());
            }

            return vResult;
        }

        /// <summary>
        /// 取得 QC檔案是否為帶審核？
        /// </summary>
        public bool GetQCFileIsChecked(string VideoID)
        {
            bool vResult = false;

            try
            {
                strSQL = "";
                strSQL += "SELECT [FSVIDEO_ID] ";
                strSQL += "FROM [TBLOG_VIDEO_HD_MC] ";
                strSQL += "WHERE [FSVIDEO_ID]='00" + VideoID + "' AND [FCSTATUS] IN ('O','X')";

                DataTable dt = dbFun.CreateDataTable(strSQL);

                if (dt.Rows.Count != 0)
                {
                    vResult = true;
                }
            }
            catch (Exception ex)
            {
                logFun.LogError("MAM_PGM_QC.QCResultBo.GetQCFileIsChecked()", ex.ToString());
            }

            return vResult;
        }

        #endregion

        #region - 新增、修改 -

        /// <summary>
        /// 資料新增(TBLOG_QC_RESULT)
        /// </summary>
        public bool Insert()
        {
            bool vResult = false;

            try
            {
                strSQL = "";
                strSQL += "INSERT INTO [TBLOG_QC_RESULT]([FSVIDEO_ID],[FSFILE_NO],[FSPROG_ID],[FSPROG_NAME],[FNEPISODE],[FSQC_DESCRIPTION],[FSCREATED_BY]) ";
                strSQL += "VALUES ('" + vo.FSVIDEO_ID + "','" + vo.FSFILE_NO + "','" + vo.FSPROG_ID + "','" + vo.FSPROG_NAME + "','" + vo.FNEPISODE + "','" + vo.FSQC_DESCRIPTION + "','" + GlobalUserData.UID + "') ";

                dbFun.EditData(strSQL);

                vResult = true;
            }
            finally
            {
            }

            return vResult;
        }

        /// <summary>
        /// 資料修改(TBLOG_VIDEO_HD_MC、TBLOG_VIDEO)
        /// </summary>
        public bool Update()
        {
            bool vResult = false;

            try
            {
                strSQL = "";
                strSQL += "UPDATE [TBLOG_VIDEO_HD_MC] ";
                strSQL += "SET [FCSTATUS]='" + vo.FCSTATUS + "', ";
                strSQL += "      [FSUPDATED_BY]='" + GlobalUserData.UID + "', ";
                strSQL += "      [FDUPDATED_DATE]=GETDATE() ";
                strSQL += "WHERE [FSFILE_NO]='" + vo.FSFILE_NO + "' ";

                dbFun.EditData(strSQL);

                if (vo.FCSTATUS.Equals(GlobalMsgTxt.NG))
                {
                    strSQL = "";
                    strSQL += "UPDATE [TBLOG_VIDEO] ";
                    strSQL += "SET [FCFILE_STATUS]='F', ";
                    strSQL += "      [FSUPDATED_BY]='" + GlobalUserData.UID + "', ";
                    strSQL += "      [FDUPDATED_DATE]=GETDATE() ";
                    strSQL += "WHERE [FSFILE_NO]='" + vo.FSFILE_NO + "' ";   

                    dbFun.EditData(strSQL);
                }
                else
                {
                    //T、Y不轉
                    if ("TY".IndexOf(vo.FCFILE_STATUS) == -1)
                    {
                        if (GetQCFileMinMaxTimeCode())
                        {
                            vo.FSFILE_PATH_H = GlobalParameters.HdUploadFilePath + vo.FSVIDEO_ID + ".mxf";
                            TranscodeBo bo = new TranscodeBo();
                            bo.SetMP4Transcode(vo);
                        }
                    }
                }

                StringBuilder strLog = new StringBuilder();
                strLog.Append("影片編號：" + vo.FSVIDEO_ID);
                strLog.Append("、檔案路徑：" + vo.FSFILE_PATH);
                strLog.Append("、低解路徑：" + vo.FSFILE_PATH_L);
                strLog.Append("、TC_IN：" + vo.FSBEG_TIMECODE_MIN);
                strLog.Append("、TC_OUT：" + vo.FSEND_TIMECODE_MAX);
                strLog.Append("、檔案編號：" + vo.FSFILE_NO);
                strLog.Append("、檔案狀態：" + vo.FCFILE_STATUS);
                strLog.Append("、檔案狀態(HD)：" + vo.FCSTATUS);
                strLog.Append("、節目編號：" + vo.FSPROG_ID);
                strLog.Append("、節目名稱：" + vo.FSPROG_NAME);
                strLog.Append("、集數：" + vo.FNEPISODE);
                strLog.Append("、解析度：" + vo.FSARC_TYPE);
                strLog.Append("、檢查人：" + GlobalUserData.CName + "(" + GlobalUserData.UID + ")");
                strLog.Append("、檢查日期：" + DateTime.Now.ToString());
                strLog.Append("、檢查結果：" + vo.FSQC_DESCRIPTION);
                strLog.Append("、電腦：" + GlobalUserData.ComputerName);
                strLog.Append("、軟體版本：" + GlobalParameters.AppVersion);

                logFun.LogTrace("MAM_PGM_QC.QCResultBo.Update()", "【QC結果】" + strLog);

                vResult = true;
            }
            catch (Exception ex)
            {
                logFun.LogError("MAM_PGM_QC.QCResultBo.Update()", ex.ToString());
            }

            return vResult;
        }

        #endregion

        #region - 欄位和屬性 -

        /// <summary>
        /// T-SQL
        /// </summary>
        private string strSQL = String.Empty;

        #endregion
    }
}