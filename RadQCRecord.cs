﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using MAM_PGM_QC.App_Code;
using MAM_PGM_QC.DataStruct;

namespace MAM_PGM_QC
{
    /// <summary>
    /// QC 檢查備註及歷史紀錄
    /// </summary>
    public partial class RadQCRecord : Telerik.WinControls.UI.RadForm
    {
        #region - 宣告 -

        QCResultBo resultBo = new QCResultBo();
        PlayListBo playlistBo = new PlayListBo();

        #endregion

        #region - 初始化 -

        public RadQCRecord()
        {
            InitializeComponent();
        }

        private void RadQCRecord_Load(object sender, EventArgs e)
        {
            SetDataToForm();
        }

        #endregion

        #region - 方法 -

        /// <summary>
        /// 資料 To 視窗畫面
        /// </summary>
        private void SetDataToForm()
        {
            //載入QC結果下拉式選單
            OptionItemsBo itemBo = new OptionItemsBo();
            radDropDownList1.DataSource = itemBo.SelectQCResultItem();
            radDropDownList1.DisplayMember = "TEXT";
            radDropDownList1.ValueMember = "VALUE";

            //載入QC紀錄
            resultBo.vo.FSVIDEO_ID = _FSVIDEO_ID;
            radGridView1.DataSource = resultBo.GetQCResultList();

            //TextBox 設定預設值
            playlistBo.vo.FDDATE = _FDDATE;
            playlistBo.vo.FSCHANNEL_ID = _FSCHANNEL_ID;
            PlayListVo vo = playlistBo.GetPlayList(_FSVIDEO_ID);

            radTextBox3.Text = (vo.FCPROPERTY == "G") ? "節目" : "短帶";  
            radTextBox4.Text = vo.FSFILE_NO;
            radTextBox5.Text = vo.FSVIDEO_ID;
            radTextBox6.Text = vo.FSPROG_NAME;
            radTextBox7.Text = vo.FNEPISODE.ToString();
            radTextBox8.Text = vo.FSMEMO;

            //登入資料
            radTextBox2.Text = GlobalUserData.CName;

            SetIsUseQC();
        }

        /// <summary>
        /// 是否開放QC功能
        /// </summary>
        private void SetIsUseQC() 
        {
            if (_FSFILE_USEQC)
            {
                radGroupBox2.Enabled = true;
            }
            else 
            {
                radGroupBox2.Enabled = false;
            }
        }

        #endregion

        #region - 事件 -

        #region Button

        /// <summary>
        /// 確定儲存
        /// </summary>
        private void radBtnSubmit_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(radDropDownList1.SelectedValue.ToString()))
            {
                //新增 TBLOG_QC_RESULT
                resultBo.vo.FSVIDEO_ID = _FSVIDEO_ID;
                resultBo.vo.FSFILE_NO = _FSFILE_NO;
                resultBo.vo.FSPROG_ID = _FSPROG_ID;
                resultBo.vo.FSPROG_NAME = _FSPROG_NAME;
                resultBo.vo.FNEPISODE = _FNEPISODE;
                resultBo.vo.FSQC_DESCRIPTION = _FSQC_DESCRIPTION;
                resultBo.vo.FSFILE_PATH = _FSFILE_PATH;
                resultBo.vo.FSFILE_PATH_L = _FSFILE_PATH_L;
                resultBo.vo.FCFILE_STATUS = _FCFILE_STATUS;

                if (!String.IsNullOrEmpty(radTextBox1.Text))
                {
                    resultBo.vo.FSQC_DESCRIPTION = radDropDownList1.SelectedItem.Text + "；" + radTextBox1.Text;
                }
                else 
                {
                    resultBo.vo.FSQC_DESCRIPTION = radDropDownList1.SelectedItem.Text;
                }
                
                resultBo.Insert();

                //更新 TBLOG_VIDEO_HD_MC
                resultBo.vo.FCSTATUS = radDropDownList1.SelectedValue.ToString();
                resultBo.Update();

                Close();
            }
            else 
            {
                MessageBox.Show("請選擇 QC 檢查結果...");
            }
        }

        #endregion

        #endregion

        #region - 欄位和屬性 -

        /// <summary>T-SQL</summary>
        private string strSQL = String.Empty;

        /// <summary>可否USE QC</summary>
        public bool FSFILE_USEQC
        {
            get { return _FSFILE_USEQC; }
            set { _FSFILE_USEQC = value; }
        }
        private bool _FSFILE_USEQC;

        /// <summary>頻道代碼</summary>
        public string FSCHANNEL_ID
        {
            get { return _FSCHANNEL_ID; }
            set { _FSCHANNEL_ID = value; }
        }
        private string _FSCHANNEL_ID;

        /// <summary>播出日期</summary>
        public string FDDATE
        {
            get { return _FDDATE; }
            set { _FDDATE = value; }
        }
        private string _FDDATE;

        /// <summary>影片編號</summary>
        public string FSVIDEO_ID
        {
          get { return _FSVIDEO_ID; }
          set { _FSVIDEO_ID = value; }
        }
        private string _FSVIDEO_ID;

        /// <summary>檔案編號</summary>
        public string FSFILE_NO
        {
            get { return _FSFILE_NO; }
            set { _FSFILE_NO = value; }
        }
        private string _FSFILE_NO;

        /// <summary>節目編號</summary>
        public string FSPROG_ID
        {
            get { return _FSPROG_ID; }
            set { _FSPROG_ID = value; }
        }
        private string _FSPROG_ID;

        /// <summary>影片名稱</summary>
        public string FSPROG_NAME
        {
            get { return _FSPROG_NAME; }
            set { _FSPROG_NAME = value; }
        }
        private string _FSPROG_NAME;

        /// <summary>集數</summary>
        public int FNEPISODE
        {
            get { return _FNEPISODE; }
            set { _FNEPISODE = value; }
        }
        private int _FNEPISODE;

        /// <summary>說明</summary>
        public string FSQC_DESCRIPTION
        {
            get { return _FSQC_DESCRIPTION; }
            set { _FSQC_DESCRIPTION = value; }
        }
        private string _FSQC_DESCRIPTION;

        /// <summary>檔案路徑</summary>
        public string FSFILE_PATH
        {
            get { return _FSFILE_PATH; }
            set { _FSFILE_PATH = value; }
        }
        private string _FSFILE_PATH;

        /// <summary>高解檔路徑</summary>
        public string FSFILE_PATH_H
        {
            get { return _FSFILE_PATH_H; }
            set { _FSFILE_PATH_H = value; }
        }
        private string _FSFILE_PATH_H;

        /// <summary>低解檔路徑</summary>
        public string FSFILE_PATH_L
        {
            get { return _FSFILE_PATH_L; }
            set { _FSFILE_PATH_L = value; }
        }
        private string _FSFILE_PATH_L;

        /// <summary>檔案狀態</summary>
        public string FCFILE_STATUS
        {
            get { return _FCFILE_STATUS; }
            set { _FCFILE_STATUS = value; }
        }
        private string _FCFILE_STATUS;

        #endregion
    }
}