﻿namespace MAM_PGM_QC
{
    partial class RadMainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadMainForm));
            this.radSplitContainer1 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel1 = new Telerik.WinControls.UI.SplitPanel();
            this.dgvPlayList = new System.Windows.Forms.DataGridView();
            this.NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSVIDEO_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSPROG_NAME = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FNEPISODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSBEG_TIMECODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSEND_TIMECODE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSARC_TYPE = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FCFILE_STATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FCSTATUS = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSFILE_EXIST = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FSSEG_STATUS = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FSFILE_USEQC = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.FSFILE_PATH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSFILE_PATH_L = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSFILE_NO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FSPROG_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.menuItemPlayList = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuItemTCIN = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemTCOUT = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemQC = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemQCResultOK = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemQCResultNG = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemQCResultNote = new System.Windows.Forms.ToolStripMenuItem();
            this.menuItemLine1 = new System.Windows.Forms.ToolStripSeparator();
            this.menuItemCopyVideoID = new System.Windows.Forms.ToolStripMenuItem();
            this.splitPanel2 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer2 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel3 = new Telerik.WinControls.UI.SplitPanel();
            this.mPreviewControl1 = new MControls.MPreviewControl();
            this.mAudioMeter1 = new MControls.MAudioMeter();
            this.splitPanel4 = new Telerik.WinControls.UI.SplitPanel();
            this.mFileSpec1 = new MControls.MFileSpec();
            this.btnGoTCOut = new System.Windows.Forms.Button();
            this.btnGoTCIn = new System.Windows.Forms.Button();
            this.tcBoxPos = new MediaBrowse.Controls.TimecodeBox();
            this.mSeekControl2 = new MControls.MFileSeeking2();
            this.mFileStateControl2 = new MControls.MFileState2();
            this.btnGoPos = new System.Windows.Forms.Button();
            this.splitPanel5 = new Telerik.WinControls.UI.SplitPanel();
            this.radSplitContainer3 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel6 = new Telerik.WinControls.UI.SplitPanel();
            this.radDrpFSCHANNEL_ID = new Telerik.WinControls.UI.RadDropDownList();
            this.radBtnPlayList = new Telerik.WinControls.UI.RadButton();
            this.picLogoFull = new System.Windows.Forms.PictureBox();
            this.radBtnPlayListReload = new Telerik.WinControls.UI.RadButton();
            this.radBtnPlayListForQC = new Telerik.WinControls.UI.RadButton();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radDtpFDDATE = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radSplitContainer4 = new Telerik.WinControls.UI.RadSplitContainer();
            this.splitPanel7 = new Telerik.WinControls.UI.SplitPanel();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).BeginInit();
            this.radSplitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).BeginInit();
            this.splitPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayList)).BeginInit();
            this.menuItemPlayList.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).BeginInit();
            this.splitPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).BeginInit();
            this.radSplitContainer2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).BeginInit();
            this.splitPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).BeginInit();
            this.splitPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).BeginInit();
            this.radSplitContainer3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).BeginInit();
            this.splitPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDrpFSCHANNEL_ID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogoFull)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayListReload)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayListForQC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDtpFDDATE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer4)).BeginInit();
            this.radSplitContainer4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radSplitContainer1
            // 
            this.radSplitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radSplitContainer1.Controls.Add(this.splitPanel1);
            this.radSplitContainer1.Controls.Add(this.splitPanel2);
            this.radSplitContainer1.Location = new System.Drawing.Point(0, 41);
            this.radSplitContainer1.Name = "radSplitContainer1";
            // 
            // 
            // 
            this.radSplitContainer1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer1.Size = new System.Drawing.Size(1775, 714);
            this.radSplitContainer1.TabIndex = 7;
            this.radSplitContainer1.TabStop = false;
            this.radSplitContainer1.Text = "radSplitContainer1";
            // 
            // splitPanel1
            // 
            this.splitPanel1.Controls.Add(this.dgvPlayList);
            this.splitPanel1.Location = new System.Drawing.Point(0, 0);
            this.splitPanel1.Name = "splitPanel1";
            // 
            // 
            // 
            this.splitPanel1.RootElement.MaxSize = new System.Drawing.Size(720, 0);
            this.splitPanel1.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel1.Size = new System.Drawing.Size(720, 714);
            this.splitPanel1.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(-0.4582405F, 0F);
            this.splitPanel1.SizeInfo.MaximumSize = new System.Drawing.Size(720, 0);
            this.splitPanel1.SizeInfo.SplitterCorrection = new System.Drawing.Size(-22, 0);
            this.splitPanel1.TabIndex = 0;
            this.splitPanel1.TabStop = false;
            this.splitPanel1.Text = "splitPanel1";
            // 
            // dgvPlayList
            // 
            this.dgvPlayList.AllowDrop = true;
            this.dgvPlayList.AllowUserToAddRows = false;
            this.dgvPlayList.AllowUserToDeleteRows = false;
            this.dgvPlayList.AllowUserToResizeRows = false;
            this.dgvPlayList.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(232)))), ((int)(((byte)(254)))));
            this.dgvPlayList.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.dgvPlayList.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPlayList.ColumnHeadersHeight = 30;
            this.dgvPlayList.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NO,
            this.FSVIDEO_ID,
            this.FSPROG_NAME,
            this.FNEPISODE,
            this.FSBEG_TIMECODE,
            this.FSEND_TIMECODE,
            this.FSARC_TYPE,
            this.FCFILE_STATUS,
            this.FCSTATUS,
            this.FSFILE_EXIST,
            this.FSSEG_STATUS,
            this.FSFILE_USEQC,
            this.FSFILE_PATH,
            this.FSFILE_PATH_L,
            this.FSFILE_NO,
            this.FSPROG_ID});
            this.dgvPlayList.ContextMenuStrip = this.menuItemPlayList;
            this.dgvPlayList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvPlayList.Location = new System.Drawing.Point(0, 0);
            this.dgvPlayList.MultiSelect = false;
            this.dgvPlayList.Name = "dgvPlayList";
            this.dgvPlayList.ReadOnly = true;
            this.dgvPlayList.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvPlayList.RowHeadersWidth = 21;
            this.dgvPlayList.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Segoe UI Symbol", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dgvPlayList.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgvPlayList.RowTemplate.Height = 24;
            this.dgvPlayList.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvPlayList.Size = new System.Drawing.Size(720, 714);
            this.dgvPlayList.TabIndex = 169;
            this.dgvPlayList.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvPlayList_CellDoubleClick);
            this.dgvPlayList.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgvPlayList_CellMouseClick);
            this.dgvPlayList.RowPrePaint += new System.Windows.Forms.DataGridViewRowPrePaintEventHandler(this.dgvPlayList_RowPrePaint);
            // 
            // NO
            // 
            this.NO.FillWeight = 60F;
            this.NO.HeaderText = "序號";
            this.NO.Name = "NO";
            this.NO.ReadOnly = true;
            this.NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.NO.Width = 40;
            // 
            // FSVIDEO_ID
            // 
            this.FSVIDEO_ID.DataPropertyName = "FSVIDEO_ID";
            this.FSVIDEO_ID.HeaderText = "影片編號";
            this.FSVIDEO_ID.Name = "FSVIDEO_ID";
            this.FSVIDEO_ID.ReadOnly = true;
            this.FSVIDEO_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSVIDEO_ID.Width = 70;
            // 
            // FSPROG_NAME
            // 
            this.FSPROG_NAME.DataPropertyName = "FSPROG_NAME";
            this.FSPROG_NAME.HeaderText = "節目 / 短帶名稱";
            this.FSPROG_NAME.Name = "FSPROG_NAME";
            this.FSPROG_NAME.ReadOnly = true;
            this.FSPROG_NAME.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSPROG_NAME.Width = 200;
            // 
            // FNEPISODE
            // 
            this.FNEPISODE.DataPropertyName = "FNEPISODE";
            this.FNEPISODE.HeaderText = "集數";
            this.FNEPISODE.Name = "FNEPISODE";
            this.FNEPISODE.ReadOnly = true;
            this.FNEPISODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FNEPISODE.Width = 55;
            // 
            // FSBEG_TIMECODE
            // 
            this.FSBEG_TIMECODE.DataPropertyName = "FSBEG_TIMECODE";
            this.FSBEG_TIMECODE.HeaderText = "IN 點";
            this.FSBEG_TIMECODE.Name = "FSBEG_TIMECODE";
            this.FSBEG_TIMECODE.ReadOnly = true;
            this.FSBEG_TIMECODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSBEG_TIMECODE.Width = 75;
            // 
            // FSEND_TIMECODE
            // 
            this.FSEND_TIMECODE.DataPropertyName = "FSEND_TIMECODE";
            this.FSEND_TIMECODE.HeaderText = "OUT 點";
            this.FSEND_TIMECODE.Name = "FSEND_TIMECODE";
            this.FSEND_TIMECODE.ReadOnly = true;
            this.FSEND_TIMECODE.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSEND_TIMECODE.Width = 75;
            // 
            // FSARC_TYPE
            // 
            this.FSARC_TYPE.DataPropertyName = "FSARC_TYPE";
            this.FSARC_TYPE.HeaderText = "解析度";
            this.FSARC_TYPE.Name = "FSARC_TYPE";
            this.FSARC_TYPE.ReadOnly = true;
            this.FSARC_TYPE.Width = 55;
            // 
            // FCFILE_STATUS
            // 
            this.FCFILE_STATUS.DataPropertyName = "FCFILE_STATUS";
            this.FCFILE_STATUS.HeaderText = "檔案狀態";
            this.FCFILE_STATUS.Name = "FCFILE_STATUS";
            this.FCFILE_STATUS.ReadOnly = true;
            this.FCFILE_STATUS.Visible = false;
            // 
            // FCSTATUS
            // 
            this.FCSTATUS.DataPropertyName = "FCSTATUS";
            this.FCSTATUS.HeaderText = "狀態";
            this.FCSTATUS.Name = "FCSTATUS";
            this.FCSTATUS.ReadOnly = true;
            this.FCSTATUS.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FCSTATUS.Width = 70;
            // 
            // FSFILE_EXIST
            // 
            this.FSFILE_EXIST.HeaderText = "存在";
            this.FSFILE_EXIST.Name = "FSFILE_EXIST";
            this.FSFILE_EXIST.ReadOnly = true;
            this.FSFILE_EXIST.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FSFILE_EXIST.Width = 40;
            // 
            // FSSEG_STATUS
            // 
            this.FSSEG_STATUS.DataPropertyName = "FSSEG_STATUS";
            this.FSSEG_STATUS.HeaderText = "SEG";
            this.FSSEG_STATUS.Name = "FSSEG_STATUS";
            this.FSSEG_STATUS.ReadOnly = true;
            this.FSSEG_STATUS.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FSSEG_STATUS.Visible = false;
            this.FSSEG_STATUS.Width = 40;
            // 
            // FSFILE_USEQC
            // 
            this.FSFILE_USEQC.HeaderText = "QC";
            this.FSFILE_USEQC.Name = "FSFILE_USEQC";
            this.FSFILE_USEQC.ReadOnly = true;
            this.FSFILE_USEQC.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.FSFILE_USEQC.Visible = false;
            this.FSFILE_USEQC.Width = 40;
            // 
            // FSFILE_PATH
            // 
            this.FSFILE_PATH.FillWeight = 80F;
            this.FSFILE_PATH.HeaderText = "影片來源路徑";
            this.FSFILE_PATH.Name = "FSFILE_PATH";
            this.FSFILE_PATH.ReadOnly = true;
            this.FSFILE_PATH.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSFILE_PATH.Width = 220;
            // 
            // FSFILE_PATH_L
            // 
            this.FSFILE_PATH_L.DataPropertyName = "FSFILE_PATH_L";
            this.FSFILE_PATH_L.HeaderText = "影片目的路徑";
            this.FSFILE_PATH_L.Name = "FSFILE_PATH_L";
            this.FSFILE_PATH_L.ReadOnly = true;
            this.FSFILE_PATH_L.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSFILE_PATH_L.Visible = false;
            this.FSFILE_PATH_L.Width = 220;
            // 
            // FSFILE_NO
            // 
            this.FSFILE_NO.DataPropertyName = "FSFILE_NO";
            this.FSFILE_NO.HeaderText = "檔案編號";
            this.FSFILE_NO.Name = "FSFILE_NO";
            this.FSFILE_NO.ReadOnly = true;
            this.FSFILE_NO.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSFILE_NO.Width = 115;
            // 
            // FSPROG_ID
            // 
            this.FSPROG_ID.DataPropertyName = "FSPROG_ID";
            this.FSPROG_ID.HeaderText = "節目 / 短帶編號";
            this.FSPROG_ID.Name = "FSPROG_ID";
            this.FSPROG_ID.ReadOnly = true;
            this.FSPROG_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.FSPROG_ID.Visible = false;
            this.FSPROG_ID.Width = 80;
            // 
            // menuItemPlayList
            // 
            this.menuItemPlayList.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemTCIN,
            this.menuItemTCOUT,
            this.menuItemQC,
            this.menuItemLine1,
            this.menuItemCopyVideoID});
            this.menuItemPlayList.Name = "contextMenuStrip1";
            this.menuItemPlayList.Size = new System.Drawing.Size(149, 98);
            // 
            // menuItemTCIN
            // 
            this.menuItemTCIN.Name = "menuItemTCIN";
            this.menuItemTCIN.Size = new System.Drawing.Size(148, 22);
            this.menuItemTCIN.Text = "至 IN 點";
            this.menuItemTCIN.Click += new System.EventHandler(this.menuItemTCIN_Click);
            // 
            // menuItemTCOUT
            // 
            this.menuItemTCOUT.Name = "menuItemTCOUT";
            this.menuItemTCOUT.Size = new System.Drawing.Size(148, 22);
            this.menuItemTCOUT.Text = "至 OUT 點";
            this.menuItemTCOUT.Click += new System.EventHandler(this.menuItemTCOUT_Click);
            // 
            // menuItemQC
            // 
            this.menuItemQC.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuItemQCResultOK,
            this.menuItemQCResultNG,
            this.menuItemQCResultNote});
            this.menuItemQC.Name = "menuItemQC";
            this.menuItemQC.Size = new System.Drawing.Size(148, 22);
            this.menuItemQC.Text = "QC 檢查結果";
            // 
            // menuItemQCResultOK
            // 
            this.menuItemQCResultOK.Name = "menuItemQCResultOK";
            this.menuItemQCResultOK.Size = new System.Drawing.Size(160, 22);
            this.menuItemQCResultOK.Text = "通過";
            this.menuItemQCResultOK.Click += new System.EventHandler(this.menuItemQCResultOK_Click);
            // 
            // menuItemQCResultNG
            // 
            this.menuItemQCResultNG.Name = "menuItemQCResultNG";
            this.menuItemQCResultNG.Size = new System.Drawing.Size(160, 22);
            this.menuItemQCResultNG.Text = "不通過";
            this.menuItemQCResultNG.Click += new System.EventHandler(this.menuItemQCResultNG_Click);
            // 
            // menuItemQCResultNote
            // 
            this.menuItemQCResultNote.Name = "menuItemQCResultNote";
            this.menuItemQCResultNote.Size = new System.Drawing.Size(160, 22);
            this.menuItemQCResultNote.Text = "備註及歷史紀錄";
            this.menuItemQCResultNote.Click += new System.EventHandler(this.menuItemQCResultNote_Click);
            // 
            // menuItemLine1
            // 
            this.menuItemLine1.Name = "menuItemLine1";
            this.menuItemLine1.Size = new System.Drawing.Size(145, 6);
            // 
            // menuItemCopyVideoID
            // 
            this.menuItemCopyVideoID.Name = "menuItemCopyVideoID";
            this.menuItemCopyVideoID.Size = new System.Drawing.Size(148, 22);
            this.menuItemCopyVideoID.Text = "複製影片編號";
            this.menuItemCopyVideoID.Click += new System.EventHandler(this.menuItemCopyVideoID_Click);
            // 
            // splitPanel2
            // 
            this.splitPanel2.Controls.Add(this.radSplitContainer2);
            this.splitPanel2.Location = new System.Drawing.Point(724, 0);
            this.splitPanel2.Name = "splitPanel2";
            // 
            // 
            // 
            this.splitPanel2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel2.Size = new System.Drawing.Size(1051, 714);
            this.splitPanel2.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0.05511588F, 0F);
            this.splitPanel2.SizeInfo.RelativeRatio = new System.Drawing.SizeF(0.5740535F, 0F);
            this.splitPanel2.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Relative;
            this.splitPanel2.SizeInfo.SplitterCorrection = new System.Drawing.Size(22, 0);
            this.splitPanel2.TabIndex = 1;
            this.splitPanel2.TabStop = false;
            this.splitPanel2.Text = "splitPanel2";
            // 
            // radSplitContainer2
            // 
            this.radSplitContainer2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.radSplitContainer2.Controls.Add(this.splitPanel3);
            this.radSplitContainer2.Controls.Add(this.splitPanel4);
            this.radSplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radSplitContainer2.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer2.Name = "radSplitContainer2";
            this.radSplitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // 
            // 
            this.radSplitContainer2.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer2.Size = new System.Drawing.Size(1051, 714);
            this.radSplitContainer2.TabIndex = 0;
            this.radSplitContainer2.TabStop = false;
            this.radSplitContainer2.Text = "radSplitContainer2";
            // 
            // splitPanel3
            // 
            this.splitPanel3.Controls.Add(this.mPreviewControl1);
            this.splitPanel3.Controls.Add(this.mAudioMeter1);
            this.splitPanel3.Location = new System.Drawing.Point(0, 0);
            this.splitPanel3.Name = "splitPanel3";
            // 
            // 
            // 
            this.splitPanel3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel3.Size = new System.Drawing.Size(1049, 590);
            this.splitPanel3.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.01502504F);
            this.splitPanel3.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, 242);
            this.splitPanel3.TabIndex = 0;
            this.splitPanel3.TabStop = false;
            this.splitPanel3.Text = "splitPanel3";
            // 
            // mPreviewControl1
            // 
            this.mPreviewControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mPreviewControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mPreviewControl1.Location = new System.Drawing.Point(60, 0);
            this.mPreviewControl1.Name = "mPreviewControl1";
            this.mPreviewControl1.Size = new System.Drawing.Size(989, 590);
            this.mPreviewControl1.TabIndex = 0;
            // 
            // mAudioMeter1
            // 
            this.mAudioMeter1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(237)))), ((int)(((byte)(242)))), ((int)(((byte)(255)))));
            this.mAudioMeter1.BackColorHi = System.Drawing.Color.FromArgb(((int)(((byte)(222)))), ((int)(((byte)(232)))), ((int)(((byte)(254)))));
            this.mAudioMeter1.ColorGainSlider = System.Drawing.Color.Red;
            this.mAudioMeter1.ColorLevelBack = System.Drawing.Color.DarkGray;
            this.mAudioMeter1.ColorLevelHi = System.Drawing.Color.Red;
            this.mAudioMeter1.ColorLevelLo = System.Drawing.Color.DarkBlue;
            this.mAudioMeter1.ColorLevelMid = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(92)))), ((int)(((byte)(144)))));
            this.mAudioMeter1.ColorLevelOrg = System.Drawing.Color.Silver;
            this.mAudioMeter1.ColorOutline = System.Drawing.Color.Black;
            this.mAudioMeter1.Dock = System.Windows.Forms.DockStyle.Left;
            this.mAudioMeter1.Location = new System.Drawing.Point(0, 0);
            this.mAudioMeter1.Margin = new System.Windows.Forms.Padding(4);
            this.mAudioMeter1.Name = "mAudioMeter1";
            this.mAudioMeter1.Size = new System.Drawing.Size(60, 590);
            this.mAudioMeter1.TabIndex = 215;
            // 
            // splitPanel4
            // 
            this.splitPanel4.BackColor = System.Drawing.SystemColors.Control;
            this.splitPanel4.Controls.Add(this.mFileSpec1);
            this.splitPanel4.Controls.Add(this.btnGoTCOut);
            this.splitPanel4.Controls.Add(this.btnGoTCIn);
            this.splitPanel4.Controls.Add(this.tcBoxPos);
            this.splitPanel4.Controls.Add(this.mSeekControl2);
            this.splitPanel4.Controls.Add(this.mFileStateControl2);
            this.splitPanel4.Controls.Add(this.btnGoPos);
            this.splitPanel4.Location = new System.Drawing.Point(0, 594);
            this.splitPanel4.Margin = new System.Windows.Forms.Padding(10);
            this.splitPanel4.Name = "splitPanel4";
            this.splitPanel4.Padding = new System.Windows.Forms.Padding(4);
            this.splitPanel4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            // 
            // 
            // 
            this.splitPanel4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel4.Size = new System.Drawing.Size(1049, 118);
            this.splitPanel4.SizeInfo.AbsoluteSize = new System.Drawing.Size(200, 118);
            this.splitPanel4.SizeInfo.AutoSizeScale = new System.Drawing.SizeF(0F, -0.216763F);
            this.splitPanel4.SizeInfo.SizeMode = Telerik.WinControls.UI.Docking.SplitPanelSizeMode.Absolute;
            this.splitPanel4.SizeInfo.SplitterCorrection = new System.Drawing.Size(0, -242);
            this.splitPanel4.TabIndex = 1;
            this.splitPanel4.TabStop = false;
            this.splitPanel4.Text = "splitPanel4";
            // 
            // mFileSpec1
            // 
            this.mFileSpec1.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.mFileSpec1.Location = new System.Drawing.Point(513, 19);
            this.mFileSpec1.Name = "mFileSpec1";
            this.mFileSpec1.Size = new System.Drawing.Size(284, 20);
            this.mFileSpec1.TabIndex = 220;
            // 
            // btnGoTCOut
            // 
            this.btnGoTCOut.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGoTCOut.Location = new System.Drawing.Point(1001, 31);
            this.btnGoTCOut.Name = "btnGoTCOut";
            this.btnGoTCOut.Size = new System.Drawing.Size(31, 72);
            this.btnGoTCOut.TabIndex = 219;
            this.btnGoTCOut.Text = "結束點";
            this.btnGoTCOut.UseVisualStyleBackColor = true;
            this.btnGoTCOut.Click += new System.EventHandler(this.btnGoTCOut_Click);
            // 
            // btnGoTCIn
            // 
            this.btnGoTCIn.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGoTCIn.Location = new System.Drawing.Point(807, 32);
            this.btnGoTCIn.Name = "btnGoTCIn";
            this.btnGoTCIn.Size = new System.Drawing.Size(31, 72);
            this.btnGoTCIn.TabIndex = 218;
            this.btnGoTCIn.Text = "起始點";
            this.btnGoTCIn.UseVisualStyleBackColor = true;
            this.btnGoTCIn.Click += new System.EventHandler(this.btnGoTCIn_Click);
            // 
            // tcBoxPos
            // 
            this.tcBoxPos.BackColor = System.Drawing.Color.Red;
            this.tcBoxPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tcBoxPos.Font = new System.Drawing.Font("微軟正黑體", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.tcBoxPos.ForeColor = System.Drawing.SystemColors.Menu;
            this.tcBoxPos.Format = MediaBrowse.AssetObjects.TimecodeFormat.DropFrame;
            this.tcBoxPos.Location = new System.Drawing.Point(844, 35);
            this.tcBoxPos.Name = "tcBoxPos";
            this.tcBoxPos.ReadOnly = true;
            this.tcBoxPos.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tcBoxPos.Rollover = true;
            this.tcBoxPos.Size = new System.Drawing.Size(150, 35);
            this.tcBoxPos.TabIndex = 0;
            this.tcBoxPos.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.tcBoxPos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tcBoxPos_KeyDown);
            // 
            // mSeekControl2
            // 
            this.mSeekControl2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.mSeekControl2.Location = new System.Drawing.Point(203, 35);
            this.mSeekControl2.Name = "mSeekControl2";
            this.mSeekControl2.Size = new System.Drawing.Size(596, 72);
            this.mSeekControl2.TabIndex = 217;
            this.mSeekControl2.TCCur = null;
            // 
            // mFileStateControl2
            // 
            this.mFileStateControl2.Location = new System.Drawing.Point(10, 14);
            this.mFileStateControl2.Name = "mFileStateControl2";
            this.mFileStateControl2.Size = new System.Drawing.Size(197, 95);
            this.mFileStateControl2.TabIndex = 216;
            // 
            // btnGoPos
            // 
            this.btnGoPos.Font = new System.Drawing.Font("微軟正黑體", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.btnGoPos.Location = new System.Drawing.Point(843, 74);
            this.btnGoPos.Name = "btnGoPos";
            this.btnGoPos.Size = new System.Drawing.Size(152, 29);
            this.btnGoPos.TabIndex = 214;
            this.btnGoPos.Text = "前往 TIMECODE 定點";
            this.btnGoPos.UseVisualStyleBackColor = true;
            this.btnGoPos.Click += new System.EventHandler(this.btnGoPos_Click);
            // 
            // splitPanel5
            // 
            this.splitPanel5.Location = new System.Drawing.Point(0, 0);
            this.splitPanel5.Name = "splitPanel5";
            // 
            // 
            // 
            this.splitPanel5.RootElement.MinSize = new System.Drawing.Size(25, 25);
            this.splitPanel5.Size = new System.Drawing.Size(200, 200);
            this.splitPanel5.TabIndex = 0;
            this.splitPanel5.TabStop = false;
            // 
            // radSplitContainer3
            // 
            this.radSplitContainer3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.radSplitContainer3.Controls.Add(this.splitPanel6);
            this.radSplitContainer3.Dock = System.Windows.Forms.DockStyle.Top;
            this.radSplitContainer3.Location = new System.Drawing.Point(0, 0);
            this.radSplitContainer3.Name = "radSplitContainer3";
            // 
            // 
            // 
            this.radSplitContainer3.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer3.Size = new System.Drawing.Size(1770, 41);
            this.radSplitContainer3.TabIndex = 8;
            this.radSplitContainer3.TabStop = false;
            this.radSplitContainer3.Text = "radSplitContainer3";
            // 
            // splitPanel6
            // 
            this.splitPanel6.BackColor = System.Drawing.SystemColors.Control;
            this.splitPanel6.Controls.Add(this.radDrpFSCHANNEL_ID);
            this.splitPanel6.Controls.Add(this.radBtnPlayList);
            this.splitPanel6.Controls.Add(this.picLogoFull);
            this.splitPanel6.Controls.Add(this.radBtnPlayListReload);
            this.splitPanel6.Controls.Add(this.radBtnPlayListForQC);
            this.splitPanel6.Controls.Add(this.radLabel2);
            this.splitPanel6.Controls.Add(this.radDtpFDDATE);
            this.splitPanel6.Controls.Add(this.radLabel1);
            this.splitPanel6.Font = new System.Drawing.Font("Segoe UI", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.splitPanel6.Location = new System.Drawing.Point(0, 0);
            this.splitPanel6.Name = "splitPanel6";
            // 
            // 
            // 
            this.splitPanel6.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel6.Size = new System.Drawing.Size(1770, 41);
            this.splitPanel6.TabIndex = 0;
            this.splitPanel6.TabStop = false;
            this.splitPanel6.Text = "splitPanel6";
            // 
            // radDrpFSCHANNEL_ID
            // 
            this.radDrpFSCHANNEL_ID.DropDownStyle = Telerik.WinControls.RadDropDownStyle.DropDownList;
            this.radDrpFSCHANNEL_ID.EnableGestures = false;
            this.radDrpFSCHANNEL_ID.Font = new System.Drawing.Font("微軟正黑體", 9F);
            this.radDrpFSCHANNEL_ID.FormattingEnabled = false;
            this.radDrpFSCHANNEL_ID.Location = new System.Drawing.Point(277, 11);
            this.radDrpFSCHANNEL_ID.Name = "radDrpFSCHANNEL_ID";
            this.radDrpFSCHANNEL_ID.ShowImageInEditorArea = false;
            this.radDrpFSCHANNEL_ID.ShowItemToolTips = false;
            this.radDrpFSCHANNEL_ID.Size = new System.Drawing.Size(96, 21);
            this.radDrpFSCHANNEL_ID.TabIndex = 188;
            this.radDrpFSCHANNEL_ID.Text = "radDropDownList1";
            // 
            // radBtnPlayList
            // 
            this.radBtnPlayList.Location = new System.Drawing.Point(573, 10);
            this.radBtnPlayList.Name = "radBtnPlayList";
            this.radBtnPlayList.Size = new System.Drawing.Size(90, 24);
            this.radBtnPlayList.TabIndex = 187;
            this.radBtnPlayList.Text = "播表清單";
            this.radBtnPlayList.Click += new System.EventHandler(this.radBtnPlayList_Click);
            // 
            // picLogoFull
            // 
            this.picLogoFull.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picLogoFull.Image = ((System.Drawing.Image)(resources.GetObject("picLogoFull.Image")));
            this.picLogoFull.InitialImage = ((System.Drawing.Image)(resources.GetObject("picLogoFull.InitialImage")));
            this.picLogoFull.Location = new System.Drawing.Point(1661, 3);
            this.picLogoFull.Name = "picLogoFull";
            this.picLogoFull.Size = new System.Drawing.Size(106, 35);
            this.picLogoFull.TabIndex = 184;
            this.picLogoFull.TabStop = false;
            // 
            // radBtnPlayListReload
            // 
            this.radBtnPlayListReload.Location = new System.Drawing.Point(477, 10);
            this.radBtnPlayListReload.Name = "radBtnPlayListReload";
            this.radBtnPlayListReload.Size = new System.Drawing.Size(90, 24);
            this.radBtnPlayListReload.TabIndex = 5;
            this.radBtnPlayListReload.Text = "重新整理";
            this.radBtnPlayListReload.Click += new System.EventHandler(this.radBtnPlayListReload_Click);
            // 
            // radBtnPlayListForQC
            // 
            this.radBtnPlayListForQC.Location = new System.Drawing.Point(382, 10);
            this.radBtnPlayListForQC.Name = "radBtnPlayListForQC";
            this.radBtnPlayListForQC.Size = new System.Drawing.Size(90, 24);
            this.radBtnPlayListForQC.TabIndex = 4;
            this.radBtnPlayListForQC.Text = "查詢";
            this.radBtnPlayListForQC.Click += new System.EventHandler(this.radBtnPlayListForQC_Click);
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel2.Location = new System.Drawing.Point(206, 11);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(68, 19);
            this.radLabel2.TabIndex = 2;
            this.radLabel2.Text = "播出頻道：";
            // 
            // radDtpFDDATE
            // 
            this.radDtpFDDATE.CustomFormat = "yyyy/MM/dd";
            this.radDtpFDDATE.EnableTheming = false;
            this.radDtpFDDATE.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.radDtpFDDATE.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDtpFDDATE.Location = new System.Drawing.Point(85, 10);
            this.radDtpFDDATE.MaxDate = new System.DateTime(2030, 12, 31, 0, 0, 0, 0);
            this.radDtpFDDATE.Name = "radDtpFDDATE";
            this.radDtpFDDATE.NullText = "- 請選擇 -";
            this.radDtpFDDATE.Size = new System.Drawing.Size(106, 21);
            this.radDtpFDDATE.TabIndex = 1;
            this.radDtpFDDATE.TabStop = false;
            this.radDtpFDDATE.Value = new System.DateTime(((long)(0)));
            ((Telerik.WinControls.UI.RadDateTimePickerElement)(this.radDtpFDDATE.GetChildAt(0))).Text = "";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("微軟正黑體", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel1.Location = new System.Drawing.Point(14, 11);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(68, 19);
            this.radLabel1.TabIndex = 0;
            this.radLabel1.Text = "播出日期：";
            // 
            // radSplitContainer4
            // 
            this.radSplitContainer4.Controls.Add(this.splitPanel7);
            this.radSplitContainer4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radSplitContainer4.Location = new System.Drawing.Point(0, 755);
            this.radSplitContainer4.Name = "radSplitContainer4";
            // 
            // 
            // 
            this.radSplitContainer4.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.radSplitContainer4.Size = new System.Drawing.Size(1770, 27);
            this.radSplitContainer4.TabIndex = 9;
            this.radSplitContainer4.TabStop = false;
            this.radSplitContainer4.Text = "radSplitContainer4";
            // 
            // splitPanel7
            // 
            this.splitPanel7.BackColor = System.Drawing.SystemColors.Control;
            this.splitPanel7.ForeColor = System.Drawing.Color.Black;
            this.splitPanel7.Location = new System.Drawing.Point(0, 0);
            this.splitPanel7.Name = "splitPanel7";
            // 
            // 
            // 
            this.splitPanel7.RootElement.MinSize = new System.Drawing.Size(0, 0);
            this.splitPanel7.Size = new System.Drawing.Size(1770, 27);
            this.splitPanel7.TabIndex = 0;
            this.splitPanel7.TabStop = false;
            this.splitPanel7.Text = "splitPanel7";
            // 
            // RadMainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1770, 782);
            this.Controls.Add(this.radSplitContainer4);
            this.Controls.Add(this.radSplitContainer3);
            this.Controls.Add(this.radSplitContainer1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "RadMainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "公共電視台 影片檔案 Quality Control 應用程式";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.RadMainForm_FormClosing);
            this.Load += new System.EventHandler(this.RadMainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer1)).EndInit();
            this.radSplitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel1)).EndInit();
            this.splitPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvPlayList)).EndInit();
            this.menuItemPlayList.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel2)).EndInit();
            this.splitPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer2)).EndInit();
            this.radSplitContainer2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel3)).EndInit();
            this.splitPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel4)).EndInit();
            this.splitPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer3)).EndInit();
            this.radSplitContainer3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel6)).EndInit();
            this.splitPanel6.ResumeLayout(false);
            this.splitPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radDrpFSCHANNEL_ID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogoFull)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayListReload)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radBtnPlayListForQC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDtpFDDATE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radSplitContainer4)).EndInit();
            this.radSplitContainer4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitPanel7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer1;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer2;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer3;
        private Telerik.WinControls.UI.RadSplitContainer radSplitContainer4;
        private Telerik.WinControls.UI.SplitPanel splitPanel1;
        private Telerik.WinControls.UI.SplitPanel splitPanel2;
        private Telerik.WinControls.UI.SplitPanel splitPanel3;
        private Telerik.WinControls.UI.SplitPanel splitPanel4;
        private Telerik.WinControls.UI.SplitPanel splitPanel5;
        private Telerik.WinControls.UI.SplitPanel splitPanel6;
        private Telerik.WinControls.UI.SplitPanel splitPanel7;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadDropDownList radDrpFSCHANNEL_ID;
        private Telerik.WinControls.UI.RadDateTimePicker radDtpFDDATE;
        private Telerik.WinControls.UI.RadButton radBtnPlayListForQC;
        private Telerik.WinControls.UI.RadButton radBtnPlayListReload;
        private Telerik.WinControls.UI.RadButton radBtnPlayList;
        private System.Windows.Forms.DataGridView dgvPlayList;
        private System.Windows.Forms.ContextMenuStrip menuItemPlayList;
        private System.Windows.Forms.ToolStripMenuItem menuItemTCIN;
        private System.Windows.Forms.ToolStripMenuItem menuItemTCOUT;
        private System.Windows.Forms.ToolStripMenuItem menuItemQC;
        private System.Windows.Forms.ToolStripMenuItem menuItemQCResultOK;
        private System.Windows.Forms.ToolStripMenuItem menuItemQCResultNG;
        private System.Windows.Forms.ToolStripMenuItem menuItemQCResultNote;
        private System.Windows.Forms.PictureBox picLogoFull;
        private System.Windows.Forms.ToolStripMenuItem menuItemCopyVideoID;
        private System.Windows.Forms.ToolStripSeparator menuItemLine1;
        private System.Windows.Forms.Button btnGoPos;
        private System.Windows.Forms.Button btnGoTCOut;
        private System.Windows.Forms.Button btnGoTCIn;
        private MediaBrowse.Controls.TimecodeBox tcBoxPos;
        private MControls.MPreviewControl mPreviewControl1;
        private MControls.MAudioMeter mAudioMeter1;
        private MControls.MFileState2 mFileStateControl2;
        private MControls.MFileSeeking2 mSeekControl2;
        private MControls.MFileSpec mFileSpec1;
        private System.Windows.Forms.DataGridViewTextBoxColumn NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSVIDEO_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSPROG_NAME;
        private System.Windows.Forms.DataGridViewTextBoxColumn FNEPISODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSBEG_TIMECODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSEND_TIMECODE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSARC_TYPE;
        private System.Windows.Forms.DataGridViewTextBoxColumn FCFILE_STATUS;
        private System.Windows.Forms.DataGridViewTextBoxColumn FCSTATUS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FSFILE_EXIST;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FSSEG_STATUS;
        private System.Windows.Forms.DataGridViewCheckBoxColumn FSFILE_USEQC;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSFILE_PATH;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSFILE_PATH_L;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSFILE_NO;
        private System.Windows.Forms.DataGridViewTextBoxColumn FSPROG_ID;

    }
}
