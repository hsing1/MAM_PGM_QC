﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using MAM_PGM_QC.App_Code;
using MAM_PGM_QC.DataStruct;

namespace MAM_PGM_QC
{
    /// <summary>
    /// QC 播表清單
    /// </summary>
    public partial class RadPlayList : Telerik.WinControls.UI.RadForm
    {
        #region - 宣告 -

        PlayListBo playlistBo = new PlayListBo();

        logFunction logFun = new logFunction();

        #endregion

        #region - 初始化 -

        public RadPlayList()
        {
            InitializeComponent();
        }

        private void RadPlayList_Load(object sender, EventArgs e)
        {
            SetDataToForm();
        }

        #endregion

        #region - 方法 -

        /// <summary>
        /// 資料 To 視窗畫面
        /// </summary>
        private void SetDataToForm()
        {
            this.Text = String.Format("公共電視台 影片檔案 Quality Control 應用程式版（{0}）", GlobalParameters.AppVersion);

            GetPlayList();
        }

        /// <summary>
        /// 載入QC表單
        /// </summary>
        private void GetPlayList()
        {
            playlistBo.vo.FSCHANNEL_ID = _FSCHANNEL_ID;
            playlistBo.vo.FDDATE = _FDDATE;

            //playlistBo.vo.FSCHANNEL_ID = "11";
            //playlistBo.vo.FDDATE = "2016/01/31";

            radGvPlayList.DataSource = playlistBo.GetPlayList();
        }

        #endregion

        #region - 事件 -

        #region RadGridView

        /// <summary>
        /// 選取 資料行 並畫面帶入QC播表
        /// </summary>
        private void radGvPlayList_CellDoubleClick(object sender, Telerik.WinControls.UI.GridViewCellEventArgs e)
        {
            foreach (Telerik.WinControls.UI.GridViewRowInfo rowInfo in radGvPlayList.Rows)
            {
                if (rowInfo.IsSelected)
                {
                    RadMainForm vForm = (RadMainForm)this.Owner;
                    vForm.GetData(rowInfo.Cells["FSVIDEO_ID"].Value.ToString(), rowInfo.Cells["FSBEG_TIMECODE"].Value.ToString().Replace(";", ":"), rowInfo.Cells["FSEND_TIMECODE"].Value.ToString().Replace(";", ":"));

                    this.Close();
                }
            }
        }

        /// <summary>
        /// 格線設定
        /// </summary>
        private void radGvPlayList_CellFormatting(object sender, Telerik.WinControls.UI.CellFormattingEventArgs e)
        {
            #region 格線設定

            e.CellElement.BorderTopWidth = 1;
            e.CellElement.BorderTopColor = Color.Silver;

            e.CellElement.BorderBottomWidth = 1;
            e.CellElement.BorderBottomColor = Color.Silver;

            e.CellElement.BorderLeftWidth = 1;
            e.CellElement.BorderLeftColor = Color.Silver;

            #endregion
        }

        /// <summary>
        /// 背景設定
        /// </summary>
        private void radGvPlayList_RowFormatting(object sender, Telerik.WinControls.UI.RowFormattingEventArgs e)
        {
            if (e.RowElement.RowInfo.Cells["FCSTATUS"].Value.Equals(GlobalMsgTxt.strOK))
            {
                e.RowElement.DrawFill = true;
                e.RowElement.GradientStyle = GradientStyles.Solid;
                e.RowElement.BackColor = Color.PaleGreen;
            }
            else if (e.RowElement.RowInfo.Cells["FCSTATUS"].Value.Equals(GlobalMsgTxt.strNG))
            {
                e.RowElement.DrawFill = true;
                e.RowElement.GradientStyle = GradientStyles.Solid;
                e.RowElement.BackColor = Color.Thistle;
            }
            else
            {
                e.RowElement.BackColor = Color.White;
            }
        }

        #endregion

        #endregion

        #region - 欄位和屬性 -

        /// <summary>頻道編號</summary>
        public string FSCHANNEL_ID
        {
            get { return _FSCHANNEL_ID; }
            set { _FSCHANNEL_ID = value; }
        }
        private string _FSCHANNEL_ID;

        /// <summary>播表日期</summary>
        public string FDDATE
        {
            get { return _FDDATE; }
            set { _FDDATE = value; }
        }
        private string _FDDATE;

        #endregion
    }
}