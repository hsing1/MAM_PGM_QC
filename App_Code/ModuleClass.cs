﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Input;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MAM_PGM_QC
{
    public static class ModuleClass
    {
        public static void getModulePermissionByUserId(string fsuser_id) 
        {
            WSUserObject.WSUserObjectSoapClient UserObj = new WSUserObject.WSUserObjectSoapClient(); 

            moduleList = UserObj.fnGetModuleList(fsuser_id);
        }

        //存放User Session 資料
        private static List<WSUserObject.ModuleStruct> moduleList = new List<WSUserObject.ModuleStruct>();

        public static Boolean getModulePermission()
        {
            Boolean brtn = false;

            foreach (WSUserObject.ModuleStruct item in moduleList)
            {
                if (item.FSMODULE_ID == "Q100001")
                {
                    brtn = true;
                    break; 
                }
            }

            return brtn; 
        }

        public static Boolean getModulePermissionByName(string fsmoudule_cat_id,string fsunc_name)
        {
            Boolean brtn = false;

            foreach (WSUserObject.ModuleStruct item in moduleList)
            {
                if ((item.FSFUNC_NAME == fsunc_name) && (item.FSMODULE_ID.Substring(0,2) == fsmoudule_cat_id) )
                {
                    brtn = true;
                    break;
                }
            }

            return brtn;
        }
    }
}
