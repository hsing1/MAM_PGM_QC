﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Input;

public static class UserClass
{
    //存放User Session 資料
    private static MAM_PGM_QC.WSASPNetSession.UserStruct m_userData;

    public static MAM_PGM_QC.WSASPNetSession.UserStruct userData
    {
        get
        {
            return m_userData; 
        }
        set
        {
            m_userData = value;
        }
    }
}

