using System;
using System.Collections.Generic;
using System.Text;

class TimeCode
{
    public static readonly int HOUR = 0;
    public static readonly int MINUTE = 1;
    public static readonly int SECOND = 2;
    public static readonly int FRAME = 3;

    string _timeCode = "00:00:00:00";

    /// <summary>
    /// 依據TimeCode Format 的字串，產生TimeCode。ex:12:30:25:15
    /// </summary>
    /// <param name="timeCode">TimeCode Format 的字串</param>
    public TimeCode(string timeCode)
    {
        this.CheckFormat(timeCode);

        this._timeCode = timeCode;
    }

    /// <summary>
    /// 依據Field種類，產生TimeCode。
    /// </summary>
    /// <param name="val">數值</param>
    /// <param name="field">TimeCode Field</param>
    public TimeCode(int val, int field)
    {
        int nField = field;
        if (TimeCode.FRAME.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val, true).ToString();
        }
        else if (TimeCode.SECOND.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val * 30, true).ToString();
        }
        else if (TimeCode.MINUTE.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val * 60 * 30, true).ToString();
        }
        else
        {
            this._timeCode = TimeCode.ToTimeCode(val * 60 * 60 * 30, true).ToString();
        }
    }

    /// <summary>
    /// 依據Field種類，產生TimeCode。
    /// 可選擇是否要drop frame Add by Yan(2009/09/04)
    /// </summary>
    /// <param name="val">數值</param>
    /// <param name="field">TimeCode Field</param>
    public TimeCode(int val, int field, bool drop)
    {
        int nField = field;
        if (TimeCode.FRAME.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val, drop).ToString();
        }
        else if (TimeCode.SECOND.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val * 30, drop).ToString();
        }
        else if (TimeCode.MINUTE.Equals(nField))
        {
            this._timeCode = TimeCode.ToTimeCode(val * 60 * 30, drop).ToString();
        }
        else
        {
            this._timeCode = TimeCode.ToTimeCode(val * 60 * 60 * 30, drop).ToString();
        }
    }

    /// <summary>
    /// 將 TimeCode 轉換為 String。ex:12:31:25:15 或 -00:03:00:00
    /// </summary>
    /// <returns>TimeCode 格式的 String</returns>
    public override String ToString()
    {
        String sHour = "";
        String sMinute = "";
        String sSecond = "";
        String sFrame = "";

        if (this.Hour.ToString().Length < 2)
        {
            sHour = "0" + this.Hour.ToString();
        }
        else
        {
            sHour = this.Hour.ToString();
        }

        if (this.Minute.ToString().Length < 2)
        {
            sMinute = "0" + this.Minute.ToString();
        }
        else
        {
            sMinute = this.Minute.ToString();
        }

        if (this.Second.ToString().Length < 2)
        {
            sSecond = "0" + this.Second.ToString();
        }
        else
        {
            sSecond = this.Second.ToString();
        }

        if (this.Frame.ToString().Length < 2)
        {
            sFrame = "0" + this.Frame.ToString();
        }
        else
        {
            sFrame = this.Frame.ToString();
        }

        if (!this.IsNegative)
        {
            return String.Format("{0}:{1}:{2}:{3}", sHour, sMinute, sSecond, sFrame);
        }
        else
        {
            return String.Format("-{0}:{1}:{2}:{3}", sHour, sMinute, sSecond, sFrame);
        }
    }

    /// <summary>
    /// 將 TimeCode 轉換為 Time。
    /// </summary>
    /// <returns></returns>
    public DateTime ToTime()
    {
        DateTime dt = new DateTime(1990,1,1);
        dt=dt.AddHours(this.Hour);
        dt=dt.AddMinutes(this.Minute);
        double dSeconds = this.Second + (this.Frame/29.97);//drop frame
        dt=dt.AddSeconds(dSeconds);

        return dt;
    }

    /// <summary>
    /// TimeCode相加
    /// </summary>
    /// <param name="timeCode">TimeCode Object</param>
    /// <returns>TimeCode Object</returns>
    public TimeCode Add(TimeCode timeCode)
    {
        int hour = this.Hour + timeCode.Hour;
        int minute = this.Minute + timeCode.Minute;
        int second = this.Second + timeCode.Second;
        int frame = this.Frame + timeCode.Frame;

        //Frame每30 Frame進位
        if (frame >= 30)
        {
            frame = frame - 30;
            second++;
        }

        //秒鐘每60秒進位
        while (second >= 60)
        {
            second = second - 60;
            minute++;
            //分鐘如果進位後，為10的倍數，則Frame不加2。
            if ((minute % 10) != 0)
            {
                //秒鐘每60秒進位，且Frame加2。
                frame = frame + 2;
                if (frame >= 30)
                {
                    frame = frame - 30;
                    second++;
                }
            }
        }

        //分鐘每60分進位
        if (minute >= 60)
        {
            minute = minute - 60;
            hour++;
        }

        return new TimeCode(String.Format("{0}:{1}:{2}:{3}", hour, minute, second, frame));
    }

    /// <summary>
    /// TimeCode相加，可選擇是否要Drop Frame。
    /// </summary>
    /// <param name="timeCode">TimeCode Object</param>
    /// <param name="drop">是否要Drop Frame</param>
    /// <returns>TimeCode Object</returns>
    public TimeCode Add(TimeCode timeCode, bool drop)
    {
        //修改為下列程式碼 Modify by Yan(2009/09/04)
        //return TimeCode.ToTimeCode(TimeCode.ToFrames(this, drop) + TimeCode.ToFrames(timeCode, drop), drop);

        if (drop)
        {
            return this.Add(timeCode);
        }
        else
        {
            return TimeCode.ToTimeCode(TimeCode.ToFrames(this, drop) + TimeCode.ToFrames(timeCode, drop), drop);
        }
    }

    /// <summary>
    /// TimeCode相減。預設不Drop Frame。
    /// </summary>
    /// <param name="timeCode">TimeCode Object</param>
    /// <returns>TimeCode Object</returns>
    /// 
    public TimeCode Subtract(TimeCode timeCode)
    {
        return this.Subtract(timeCode, false);
    }

    /// <summary>
    /// imeCode相減，可選擇是否要Drop Frame。
    /// </summary>
    /// <param name="timeCode">TimeCode Object</param>
    /// <param name="drop">是否要Drop Frame</param>
    /// <returns>TimeCode Object</returns>
    public TimeCode Subtract(TimeCode timeCode, bool drop)
    {
        return TimeCode.ToTimeCode(TimeCode.ToFrames(this, drop) - TimeCode.ToFrames(timeCode, drop), drop);
    }

    protected void CheckFormat(string timeCode)
    {
        string[] values = timeCode.Split(":".ToCharArray());
        if (values.Length != 4)
        {
            throw new FormatException("Time Code fromat must be hh:mm:ss:ff.");
        }
        else if ((Int32.Parse(values[0]) > 60))
        {
            throw new FormatException("Invalid hour format.[" + values[0] + "]");
        }
        else if ((Int32.Parse(values[1]) < 0) || (Int32.Parse(values[1]) > 59))//60
        {
            throw new FormatException("Invalid minute format.[" + values[1] + "]");
        }
        else if ((Int32.Parse(values[2]) < 0) || (Int32.Parse(values[2]) > 59))//60
        {
            throw new FormatException("Invalid second format.[" + values[2] + "]");
        }
        else if ((Int32.Parse(values[3]) < 0) || (Int32.Parse(values[3]) > 29))//30
        {
            throw new FormatException("Invalid frame format.[" + values[3] + "]");
        }
    }

    public static void CheckTimeCodeFormat(string timeCode)
    {
        string[] values = timeCode.Split(":".ToCharArray());
        if (values.Length != 4)
        {
            throw new FormatException("TimeCode輸入之格式須符合[hh:mm:ss:ff]!");
        }
        else if ((Int32.Parse(values[0]) > 60))
        {
            throw new FormatException("輸入[小時]之數值不可大於60!");
        }
        else if ((Int32.Parse(values[1]) < 0) || (Int32.Parse(values[1]) > 59))//60
        {
            throw new FormatException("輸入[分鐘]之數值不可大於等於60!");
        }
        else if ((Int32.Parse(values[2]) < 0) || (Int32.Parse(values[2]) > 59))//60
        {
            throw new FormatException("輸入[秒數]之數值不可大於等於60!");
        }
        else if ((Int32.Parse(values[3]) < 0) || (Int32.Parse(values[3]) >= 28))//30
        {
            throw new FormatException("輸入[格]之數值不可大於等於28!");
        }
    }

    /// <summary>
    /// 是否為負值
    /// </summary>
    public bool IsNegative
    {
        get
        {
            string[] values = this._timeCode.Split(":".ToCharArray());
            if (values[0].StartsWith("-"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    /// <summary>
    /// 時
    /// </summary>
    public int Hour
    {
        get
        {
            string[] values = this._timeCode.Split(":".ToCharArray());
            return System.Math.Abs(Int32.Parse(values[0]));
        }
    }

    /// <summary>
    /// 分
    /// </summary>
    public int Minute
    {
        get
        {
            string[] values = this._timeCode.Split(":".ToCharArray());
            return Int32.Parse(values[1]);
        }
    }

    /// <summary>
    /// 秒
    /// </summary>
    public int Second
    {
        get
        {
            string[] values = this._timeCode.Split(":".ToCharArray());
            return Int32.Parse(values[2]);
        }
    }

    /// <summary>
    /// 格
    /// </summary>
    public int Frame
    {
        get
        {
            string[] values = this._timeCode.Split(":".ToCharArray());
            return Int32.Parse(values[3]);
        }
    }

    /// <summary>
    /// 依據 TimeCode 取得總 Frame 數，可選擇是否要 Drop Frame。
    /// </summary>
    /// <param name="val">TimeCode Object</param>
    /// <param name="drop">是否要 Drop Frame</param>
    /// <returns>Frame 數</returns>
    public static int ToFrames(TimeCode val, bool drop)
    {
        if (drop)
        {
            //每小時減 120 Frame
            int dropFrame = val.Hour * 120;

            //每10分、20分、30分、40分、50分、60分，加 2 frame。
            dropFrame = dropFrame - (((val.Hour * 60) + val.Minute) / 10) * 2;

            return ((((((val.Hour * 60) + val.Minute) * 60) + val.Second) * 30) + val.Frame) - dropFrame;
        }
        else
        {
            return ((((((val.Hour * 60) + val.Minute) * 60) + val.Second) * 30) + val.Frame);
        }
    }

    /// <summary>
    /// 依據 Frame 取得總 TimeCode 數，可選擇是否要 Drop Frame。
    /// </summary>
    /// <param name="val">Frame</param>
    /// <param name="drop">是否要 Drop Frame</param>
    /// <returns>TimeCode 數</returns>
    public static TimeCode ToTimeCode(int val, bool drop)
    {
        Console.WriteLine("### frames:" + val);

        bool negative = false;

        if (val < 0)
        {
            negative = true;
        }

        int nVal = Math.Abs(val);

        if (drop)
        {
            //Modify by Yan (2009/06/19)
            //int dropFrameHour = (int)(nVal / (29.97 * 60 * 60));

            //int dropFrameMinute = (int)(nVal / (29.97 * 60));

            //int nonDropFrame = nVal + (dropFrameHour * 120) - ((dropFrameMinute / 10) * 2);

            //int hour = (nonDropFrame / (60 * 60 * 30));
            //int minute = (nonDropFrame - (hour * 60 * 60 * 30)) / (60 * 30);
            //int second = (nonDropFrame - (hour * 60 * 60 * 30) - (minute * 60 * 30)) / 30;
            //int frame = nonDropFrame - (hour * 60 * 60 * 30) - (minute * 60 * 30) - second * 30;

            //Modify by Yan (2009/09/04)
            //int ss = (int)(nVal / 29.97);
            //int hour = (int)(ss / (60 * 60));
            //int minute = (int)((ss - (hour * 60 * 60)) / 60);
            //int second = ss - (hour * 60 * 60) - (minute * 60);
            //int frame = (int)(((nVal / 29.97) - ss) * 29.97); //(int)Math.Round(((nVal / 29.97) - ss) * 29.97);

            //if (frame == 28)
            //{
            //    second++;
            //    frame = 0;
            //}

            //if (frame >= 29)
            //{
            //    second++;
            //    frame = frame - 29;
            //}

            //if (frame > 0 && frame <= 1)
            //{
            //    frame = frame + 1;
            //}

            //if (second >= 60)
            //{
            //    minute++;
            //    second = second - 60;
            //}
            //if (minute >= 60)
            //{
            //    hour++;
            //    minute = minute - 60;
            //}

            double ss = nVal / 29.97;
            double hour = ss / (60 * 60);
            double minute = (hour - Math.Floor(hour)) * 60;
            double second = (minute - Math.Floor(minute)) * 60;
            double frame = (second - Math.Floor(second)) * 29.97;
            //double ff = (frame - Math.Floor(frame));
            //ff = Math.Round(ff, 0);

            if (frame >= 28)
            {
                second++;
                frame = 0;
            }

            if (second >= 60)
            {
                minute++;
                second = second - 60;
            }

            if (minute >= 60)
            {
                hour++;
                minute = minute - 60;
            }

            if (negative)
            {
                return new TimeCode(String.Format("-{0}:{1}:{2}:{3}", Math.Floor(hour), Math.Floor(minute), Math.Floor(second), Math.Floor(frame)));
            }
            else
            {
                return new TimeCode(String.Format("{0}:{1}:{2}:{3}", Math.Floor(hour), Math.Floor(minute), Math.Floor(second), Math.Floor(frame)));
            }
        }
        else
        {
            int hour = (nVal / (60 * 60 * 30));
            int minute = (nVal - (hour * 60 * 60 * 30)) / (60 * 30);
            int second = (nVal - (hour * 60 * 60 * 30) - (minute * 60 * 30)) / 30;
            int frame = nVal - (hour * 60 * 60 * 30) - (minute * 60 * 30) - second * 30;

            if (negative)
            {
                return new TimeCode(String.Format("-{0}:{1}:{2}:{3}", hour, minute, second, frame));
            }
            else
            {
                return new TimeCode(String.Format("{0}:{1}:{2}:{3}", hour, minute, second, frame));
            }
        }
    }

    //Added by Verdad 2012/8/25 Issue No.22
    /// <summary>
    /// 將TimeCode以16進位記錄後轉為10進位數字
    /// </summary>
    /// <param name="timeCode">TimeCode</param>
    /// <returns>int</returns>
    public static int TimeCodeString16ToInt32(String timeCode)
    {
        return Convert.ToInt32(timeCode.Replace(":", ""), 16);
    }

    //Added by Verdad 2012/8/25 Issue No.22
    /// <summary>
    /// 將TimeCode以10進位記錄後轉為16進位數字,並轉換為TimeCode格式
    /// </summary>
    /// <param name="timeCode">TimeCode</param>
    /// <returns>int</returns>
    public static TimeCode TimeCodeInt32ToString16(int val)
    {
        //10進位轉16進位
        int remainder;
        string hex = "";
        do
        {
            remainder = val % 16;
            val /= 16;
            if (remainder < 10)
            {
                hex = remainder + hex;
            }
            else
            {
                switch (remainder)
                {
                    case 10: hex += "A"; break;
                    case 11: hex += "B"; break;
                    case 12: hex += "C"; break;
                    case 13: hex += "D"; break;
                    case 14: hex += "E"; break;
                    case 15: hex += "F"; break;
                }
            }
        } while (val >= 16);

        if (val != 0)
        {
            hex = val + hex;
        }

        //轉換TimeCode格式
        int zeoCount = 8 - hex.Length;
        for (int i = 1; i <= zeoCount; i++)
        {
            hex = "0" + hex;
        }

        return new TimeCode(String.Format("{0}:{1}:{2}:{3}", hex.Substring(0, 2), hex.Substring(2, 2), hex.Substring(4, 2), hex.Substring(6, 2)));
    }
}