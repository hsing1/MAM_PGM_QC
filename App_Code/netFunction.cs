﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Security.Permissions;

/// <summary>
/// 連接網路上的共享資料夾
/// </summary>
public class netFunction
{
    #region - 宣告 -

    logFunction logFun = new logFunction();

    /// <summary>
    /// 登入
    /// </summary>
    [DllImport("advapi32.dll", SetLastError = true)]
    public static extern bool LogonUser(
        string lpszUsername,
        string lpszDomain,
        string lpszPassword,
        int dwLogonType,
        int dwLogonProvider,
        ref IntPtr phToken
    );

    /// <summary>
    /// 登出
    /// </summary>
    [DllImport("kernel32.dll")]
    public extern static bool CloseHandle(IntPtr hToken);

    #endregion

    #region - 方法 -

    /// <summary>
    /// 網路芳鄰連線(帶帳號、密碼)
    /// </summary>
    public bool SetConnection()
    {
        bool IsConnection = false;

        try
        {
            const int LOGON32_PROVIDER_DEFAULT = 0;
            const int LOGON32_LOGON_NEW_CREDENTIALS = 9;
            IntPtr tokenHandle = new IntPtr(0);
            tokenHandle = IntPtr.Zero;

            //將登入的Token放在tokenHandle
            bool returnValue = LogonUser(
                _UserID,
                _UserDomain,
                _UserPW,
                LOGON32_LOGON_NEW_CREDENTIALS,
                LOGON32_PROVIDER_DEFAULT,
                ref tokenHandle
            );

            //讓程式模擬登入的使用者
            WindowsIdentity w = new WindowsIdentity(tokenHandle);
            w.Impersonate();

            if (false == returnValue)
            {
                logFun.LogError("MAM_PGM_QC.netFunction.SetConnection()", "無法連線至 " + _UNCPath + "。請檢查網路狀態是否正常！");

                IsConnection = false;
            }
            else 
            {
                IsConnection = true;
            }
        }
        catch (Exception ex)
        {
            logFun.LogError("MAM_PGM_QC.netFunction.SetConnection()", "網路芳鄰連線錯誤，原因：" + ex.ToString());
        }

        return IsConnection;
    }

    #endregion

    #region - 欄位和屬性 -

    /// <summary>網域</summary>
    public string UserDomain
    {
        get { return _UserDomain; }
        set { _UserDomain = value; }
    }
    private string _UserDomain;

    /// <summary>帳號</summary>
    public string UserID
    {
        get { return _UserID; }
        set { _UserID = value; }
    }
    private string _UserID;

    /// <summary>密碼</summary>
    public string UserPW
    {
        get { return _UserPW; }
        set { _UserPW = value; }
    }
    private string _UserPW;

    /// <summary>UNC路徑</summary>
    public string UNCPath
    {
        get { return _UNCPath; }
        set { _UNCPath = value; }
    }
    private string _UNCPath;

    #endregion
}