﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MAM_PGM_QC.App_Code;
using MAM_PTS_DLL;

/// <summary> 
/// LOG紀錄
/// </summary> 
public class logFunction
{
    /// <summary>
    ///  ERROR
    /// </summary>
    public void LogError(string strFunName, string strMsg)
    {
        bool IsWrite = true;

        if (IsWrite)
        {
            Log.AppendTrackingLog(strFunName, Log.TRACKING_LEVEL.ERROR, strMsg);
        }
    }

    /// <summary>
    ///  TRACE
    /// </summary>
    public void LogTrace(string strFunName, string strMsg)
    {
        bool IsWrite = true;

        if (IsWrite)
        {
            Log.AppendTrackingLog(strFunName, Log.TRACKING_LEVEL.TRACE, strMsg);
        }
    }

    /// <summary>
    ///  INFO
    /// </summary>
    public void LogInfo(string strFunName, string strMsg)
    {
        bool IsWrite = true;

        if (IsWrite)
        {
            Log.AppendTrackingLog(strFunName, Log.TRACKING_LEVEL.INFO, strMsg);
        }
    }

    #region - 欄位和屬性 -

    /// <summary>
    /// T-SQL
    /// </summary>
    private string strSQL = String.Empty;

    #endregion
}