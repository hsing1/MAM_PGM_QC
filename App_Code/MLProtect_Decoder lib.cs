//---------------------------------------------------------------------------
// MLProtect_Decoderlib.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call DecoderlibLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class DecoderlibLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={325A978E-AEDD-416C-93E0-7C02142F4DDC}
License.Key={74A7C5B5-E3C4-69AF-A1C6-1A454A4D2EA8}
License.Name=MPlaylist Module
License.UpdateExpirationDate=October 14, 2016
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=8A3068FFA68325B5E1DC76E5D7904D8CF6C74343BF691EC0FC8FC3A8772723D70E310C5AAB355721179A6B351462B7F7606070FB807D50BA791D5EF93F79F4792C06E357735FBB5B0EB455E493CF07F3BD7322D4F52E3BDD3EEAD45823A674D514F3E41D38BE33EC95184F953EEB622642E28CDFBF150D636FCA09A4263F2818

[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={17726C27-E8AE-4823-92E6-2AA58C3C4D53}
License.Key={4A762A78-1818-7712-9CA6-04571FBE9C3D}
License.Name=MFile Module
License.UpdateExpirationDate=October 14, 2016
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=52FC613F6064949848BE5DA2E827FBF95A7CE0B1DF64E8E9B08FE2C949664AAAF535ACD1009EEB7943211F99D057D45B724B0A5153054C4F54D405A0A36D43C7B0D6E1C748D61D2B32B788AC3C2CA0993BCA69B75F9D4EAF08F0CB7FFEAE76AFB88B73FEED87802D7E3AB6EBC3C0E1A6AA7E964BEB2296A1581F3CBAEDAB6C11

[MediaLooks]
License.ProductName=Decoder lib
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={13086C7B-1FA7-4886-B0C6-FA7C486FAE7B}
License.Key={8C9DC535-B1CF-DA0F-2D10-389E2250481A}
License.Name=MMixer Module (BETA)
License.UpdateExpirationDate=October 14, 2016
License.Edition=MDecoders
License.AllowedModule=*.*
License.Signature=2960EB2CEF0FE837511163D67BAFC7F9109E8F733BD836595C2636F51D1A49DFA91E39A622744CA3E1FCDB5B3F5AA989A3E7A8C8B8BD456179EC41BE6A7F47337A3FB7BB814FFABF0536E6B50DDCCCC883A884E07CAABF35FFBD902E3D010269DA3E758C9F5519E3FD1BB0C1BD02BC146D06106247D36344FF99E3C891FD7584

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Formosa Television Co.Ltd.
            const long _Q1_ = 47742703;
            const long _P1_ = 51237101;
            const long _Q2_ = 51379201;
            const long _P2_ = 46832743;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }