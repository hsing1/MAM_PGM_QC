﻿using System;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Globalization;
using System.Text;
using System.Windows.Forms;
using MAM_PGM_QC.DataStruct;
using Microsoft.Win32;
using Xealcom.Security;

namespace MAM_PGM_QC.App_Code
{
    /// <summary> 
    /// 共用參數設定
    /// </summary> 
    public class GlobalParameters
    {
        #region - 設定 -

        #region 測試 網路芳鄰

        //本機
        //public static string PTSUserDomain = @"\";
        //public static string PTSUserName = "JamesHo";
        //public static string PTSPassword = "1qaz2wsx";

        //public static string HdUploadFilePath = @"\\172.20.141.46\MXF\HDUpload\";
        //public static string WaitQCFilePath = @"\\172.20.141.46\MXF\Review\";
        //public static string DoneQCFilePath = @"\\172.20.141.46\MXF\Approved\";

        //公視
        //public static string PTSUserDomain = "pts";
        //public static string PTSUserName = "mamsystem";
        //public static string PTSPassword = "p@ssw0rd";

        //public static string HdUploadFilePath = @"\\10.13.200.6\TEST\HDUpload\";
        //public static string WaitQCFilePath = @"\\10.13.200.6\TEST\Review\";
        //public static string DoneQCFilePath = @"\\10.13.200.6\Approved\";


        #endregion

        #region 正式 網路芳鄰

        public static string PTSUserDomain = "pts";
        public static string PTSUserName = "mamsystem";
        public static string PTSPassword = "p@ssw0rd";


        public static string HdUploadFilePath;
        public static string WaitQCFilePath;
        public static string DoneQCFilePath;

        //public static string HdUploadFilePath = @"\\10.13.200.6\HDUpload\";
        //public static string WaitQCFilePath = @"\\10.13.200.6\Review\";
        //public static string DoneQCFilePath = @"\\10.13.200.6\Approved\";

        #endregion

        #region 其他參數

        //public static string LogPath = Application.StartupPath + @"\Log\";
        //public static string InfoPath = Application.StartupPath + @"\Info\";
        //public static string EventPath = Application.StartupPath + @"\Event\";

        //public static string ApplicationSaveTempFolder = Application.StartupPath + @"\Temp\";

        public static string LogPath = @"C:\PTS\MAM_PGM_QC\Log\";
        public static string InfoPath = @"C:\PTS\MAM_PGM_QC\Info\";
        public static string EventPath = @"C:\PTS\MAM_PGM_QC\Event\";

        public static string ApplicationSaveTempFolder = @"C:\PTS\MAM_PGM_QC\Temp\";

        #endregion

        #endregion

        #region - 共用方法 -

        /// <summary>
        /// 布林 轉 int
        /// </summary>
        /// <returns>0 或 1</returns>
        public static int BoolToInt(bool TrueOrFalse)
        {
            int i = TrueOrFalse ? 1 : 0;

            return i;
        }

        /// <summary>
        /// 日期 NULL 轉 空值
        /// </summary>
        /// <param name="dateTime">日期</param>
        public static string DateTimeNullToEmpty(DateTime dateTime)
        {
            if (DateTime.Compare(dateTime, DateTime.MinValue) == 0)
            {
                return "";
            }
            else
            {
                return dateTime.ToString();
            }
        }

        /// <summary>
        /// 時分秒 轉 總秒數
        /// </summary>
        /// <param name="時分秒">hh:mm:ss</param>
        /// <returns>秒數</returns>
        public static string HHmmssToSec(DateTime Duration)
        {
            TimeSpan ts = new TimeSpan(0, Duration.Hour, Duration.Minute, Duration.Second, 0);                 

            return ts.TotalSeconds.ToString();
        }

        /// <summary>
        /// 取得 檔案列表(限 MXF)
        /// </summary>
        /// <param name="path">資料夾路徑</param>
        public static DataTable GetFilePathList(string FilePath)
        {
            DataTable dt = new DataTable();

            try
            {
                netFunction netFun = new netFunction();

                netFun.UNCPath = FilePath;
                netFun.UserDomain = GlobalParameters.PTSUserDomain;
                netFun.UserID = GlobalParameters.PTSUserName;
                netFun.UserPW = GlobalParameters.PTSPassword;
                netFun.SetConnection();

                var files =
                from file in Directory.EnumerateFiles(FilePath, "*", SearchOption.AllDirectories)
                .Where(f =>
                {
                    string ext = Path.GetExtension(f).ToLower();

                    if (".mxf".Equals(ext))
                    {
                        return true;
                    }

                    return false;
                })
                select new
                {
                    fileName = Path.GetFileNameWithoutExtension(file),
                    filePath = Path.GetFullPath(file)
                };

                dt.Columns.Add(new DataColumn("fileName", typeof(string)));
                dt.Columns.Add(new DataColumn("filePath", typeof(string)));

                foreach (var s in files)
                {
                    DataRow dr = dt.NewRow();
                    dr["fileName"] = s.fileName;
                    dr["filePath"] = s.filePath;
                    dt.Rows.Add(dr);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("無法連線錯誤，原因： " + ex.ToString());
            }

            return dt;
        }

        #region 取得 應用程式版本

        /// <summary>
        /// 應用程式版本
        /// </summary>
        public static string AppVersion = GetAppVersionRegistry();

        /// <summary>
        /// 取得 目前應用程式版本 (Client端)
        /// </summary>
        private static string GetAppVersionRegistry()
        {
            #region 機碼(取消使用)

            //Microsoft.Win32.RegistryKey RegistryRoot = Microsoft.Win32.Registry.LocalMachine;
            //Microsoft.Win32.RegistryKey subkey = RegistryRoot.OpenSubKey(@"SOFTWARE\Wow6432Node\PTS\MAM_PGM_QC\");
            //string CurrentVersion = String.Empty;

            //if (subkey != null)
            //{
            //    string[] valueNames = subkey.GetValueNames();

            //    if (valueNames.Length != 0)
            //    {
            //        CurrentVersion = subkey.GetValue(valueNames.Where(a => a == "Version").FirstOrDefault()).ToString();
            //    }
            //}

            #endregion

            string CurrentVersion = String.Empty;
            List<string> files = Directory.GetFiles(ApplicationSaveTempFolder, "MAM_PGM_QC_*.zip", SearchOption.TopDirectoryOnly).ToList();

            if (files.Count > 0)
            {
                List<string> versionList = files.Select(a => { a = System.IO.Path.GetFileNameWithoutExtension(a).Replace("MAM_PGM_QC_", ""); return a; }).OrderByDescending(i => i).ToList();
                CurrentVersion = versionList[0];
            }

            return CurrentVersion;
        }

        #endregion

        #endregion
    }

    /// <summary>
    /// 登入者資料
    /// </summary>
    public class GlobalUserData
    {
        #region - 欄位和屬性 -

        /// <summary>帳號</summary>
        public static string UID;
        /// <summary>帳號名稱</summary>
        public static string CName;
        /// <summary>電腦名稱</summary>
        public static string ComputerName;
        /// <summary>是否啟用</summary>
        public static string IsActive;

        #endregion
    }

    /// <summary>
    /// 訊息處理
    /// </summary>
    public class GlobalMsgBox
    {
        /// <summary>
        /// 處理訊息 YES、NO
        /// </summary>
        /// <param name="vMessage">訊息</param>
        /// <param name="vCaption">標題</param>
        public static bool ConfirmYesNo(string vMessage, string vCaption)
        {
            if (MessageBox.Show(vMessage, vCaption, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 處理訊息 OK
        /// </summary>
        /// <param name="vMessage">訊息</param>
        /// <param name="vCaption">標題</param>
        public static void ConfirmOK(string vMessage, string vCaption)
        {
            MessageBox.Show(vMessage, vCaption, MessageBoxButtons.OK);
        }

        /// <summary>
        /// 清除畫面的資料
        /// </summary>
        /// <param name="parent"></param>
        public static void ClearForm(Control parent)
        {
            foreach (Control ctrControl in parent.Controls)
            {
                if (object.ReferenceEquals(ctrControl.GetType(), typeof(TextBox)))
                {
                    ((TextBox)ctrControl).Text = string.Empty;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(RichTextBox)))
                {
                    ((RichTextBox)ctrControl).Text = string.Empty;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(ComboBox)))
                {
                    ((ComboBox)ctrControl).SelectedIndex = 0;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(CheckBox)))
                {
                    ((CheckBox)ctrControl).Checked = false;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(RadioButton)))
                {
                    ((RadioButton)ctrControl).Checked = false;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(MaskedTextBox)))
                {
                    ((MaskedTextBox)ctrControl).Text = string.Empty;
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(ListBox)))
                {
                    if (((ListBox)ctrControl).DataSource == null)
                    {
                        ((ListBox)ctrControl).Items.Clear();
                    }
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(CheckedListBox)))
                {
                    for (int i = 0; i <  ((CheckedListBox)ctrControl).Items.Count; i++) 
                    {
                         ((CheckedListBox)ctrControl).SetItemCheckState(i, CheckState.Unchecked);
                    }             
                }
                else if (object.ReferenceEquals(ctrControl.GetType(), typeof(DateTimePicker)))
                {
                    ((DateTimePicker)ctrControl).Text =DateTime.Today.ToString();
                }

                if (ctrControl.Controls.Count > 0)
                {
                    ClearForm(ctrControl);
                }
            }
        }
    }

    /// <summary>
    /// 訊息處理文字
    /// </summary>
    public class GlobalMsgTxt
    {
        /// <summary>新增成功</summary>
        public const string InsertSuccess = "新增成功";
        /// <summary>新增失敗</summary>
        public const string InsertFail = "新增失敗";
        /// <summary>刪除確認</summary>
        public const string DeleteConfirm = "確定刪除資料？";
        /// <summary>刪除成功</summary>
        public const string DeleteSuccess = "刪除成功";
        /// <summary>刪除失敗</summary>
        public const string DeleteFail = "刪除失敗";
        /// <summary>編輯成功</summary>
        public const string UpdateSuccess = "編輯成功";
        /// <summary>編輯失敗</summary>
        public const string UpdateFail = "編輯失敗";

        //TITLE
        /// <summary>警告</summary>
        public const string WarningTitle = "警告";
        /// <summary>錯誤</summary>
        public const string ErrorTitle = "錯誤";
        /// <summary>刪除</summary>
        public const string DeleteConfirmTitle = "刪除";
        /// <summary>刪除</summary>
        public const string ReplaceConfirmTitle = "刪除";
        /// <summary>訊息</summary>
        public const string InfoTitle = "訊息";

        //INFO
        /// <summary>存檔完成</summary>
        public const string SaveCompletedInfo = "存檔完成！";
        /// <summary>刪除成功</summary>
        public const string DeleteCompletedInfo = "刪除成功！";
        //MSG
        /// <summary>確定刪除</summary>
        public const string DeleteConfirmMsg = "確定刪除？";
        /// <summary>請填寫必要欄位</summary>
        public const string RequiredMsg = "請填寫必要欄位：[" + "{0}" + "]！";
        //NOT
        /// <summary>資料無法寫入</summary>
        public const string NotInster = "資料無法寫入。";
        /// <summary>資料無法讀取，發生訊息為</summary>
        public const string NotRead = "{0} 資料無法讀取，發生訊息為：{0}。";
        /// <summary>資料無法刪除</summary>
        public const string NotDelete = "資料無法刪除。";

        /// <summary>待審核(代碼)</summary>
        public static string NA = "N";
        /// <summary>審核通過(代碼)</summary>
        public static string OK = "O";
        /// <summary>審核不通過(代碼)</summary>
        public static string NG = "X";

        /// <summary>待審核</summary>
        public static string strNA = "待審核";
        /// <summary>審核通過</summary>
        public static string strOK = "審核通過";
         /// <summary>審核不通過</summary>
        public static string strNG = "審核不過";  

        /// <summary>
        /// 設定 資料表 代碼轉換
        /// </summary>
        public static Hashtable CodeToMsg(string CodeType)
        {
            DataTable dt = new DataTable();
            Hashtable ht = new Hashtable();

            try
            {
                dt.Columns.Add("CodeType", Type.GetType("System.String"));
                dt.Columns.Add("Status", Type.GetType("System.String"));
                dt.Columns.Add("Description", Type.GetType("System.String"));

                dt.Rows.Add("PROPERTY", "G", "節目");
                dt.Rows.Add("PROPERTY", "P", "短帶");

                dt.Rows.Add("LIVE", "0", "");
                dt.Rows.Add("LIVE", "1", "LIVE");

                dt.Rows.Add("VIDEO", "B", "已送單");
                dt.Rows.Add("VIDEO", "D", "置換刪除");
                dt.Rows.Add("VIDEO", "F", "檔案待置換");
                dt.Rows.Add("VIDEO", "R", "低解轉檔失敗");
                dt.Rows.Add("VIDEO", "T", "已有高低解");
                dt.Rows.Add("VIDEO", "X", "刪除");
                dt.Rows.Add("VIDEO", "Y", "完成入庫");

                dt.Rows.Add("VIDEO_HDMC", "", "檔案未到");
                dt.Rows.Add("VIDEO_HDMC", "S", "檔案未到");
                dt.Rows.Add("VIDEO_HDMC", "C", "檔案未到");
                dt.Rows.Add("VIDEO_HDMC", "E", "檔案未到");
                dt.Rows.Add("VIDEO_HDMC", "N", "待審核");
                dt.Rows.Add("VIDEO_HDMC", "O", "審核通過");
                dt.Rows.Add("VIDEO_HDMC", "X", "審核不過");

                var distinctRows =
                (
                    from DataRow dr in dt.Rows
                    where dr.Field<string>("CodeType") == CodeType
                    select new
                    {
                        Status = dr["Status"],
                        Description = dr["Description"]
                    }
                ).Distinct();

                foreach (var od in distinctRows)
                {
                    ht.Add(od.Status, od.Description);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return ht;
        }
    }
}