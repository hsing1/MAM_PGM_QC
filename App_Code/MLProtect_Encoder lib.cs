//---------------------------------------------------------------------------
// MLProtect_Encoderlib.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call EncoderlibLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class EncoderlibLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=Encoder lib
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={AA1F7511-473E-4C98-AEDF-91CCAFD1E96D}
License.Key={D30F0CEF-AA0D-2E9D-8863-B5F39A7E778A}
License.Name=MRenderer Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=MWriter
License.AllowedModule=*.*
License.Signature=8C1C6FFE221404867884E9408380D1B4998A238657FB1ADF95D7B01BA394C448130D71F245BD21B7C24192FFB114CFF8F817050EC04385E5DE2582DAA6B051DB9475A32556986CA34DBE1A67D4F8B5FA212ED8E9875082FA0428C04982F682D5886CD8F1BAD01238BD624BBF6935AFDADB1C44EE319B9000E249F68756EA8344

[MediaLooks]
License.ProductName=Encoder lib
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={E03F5D6D-B1F3-4BF2-865C-396EA2FA7D70}
License.Key={18BAE37F-1314-02FB-7196-E50DED6EFE11}
License.Name=MWriter Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=MWriter
License.AllowedModule=*.*
License.Signature=58187CA261946497F39F7201D8B0D3203F9F2D00EB7BB03D15257D6ECE86520E3EEE8D4CFC21BDE09E65AE3DDD57D33718C8634F28E822AECFE0D92A1147B438DE97769692C5F47AE36750422787C775EAEE86CB8043AB3B36015BD0F2425939A3E09CA384E2E84617EA74F550164E0EB9C129037BADABD3E8598A99B235565C

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Formosa Television Co.Ltd.
            const long _Q1_ = 47742703;
            const long _P1_ = 51237101;
            const long _Q2_ = 51379201;
            const long _P2_ = 46832743;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }