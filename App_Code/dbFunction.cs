﻿using System;
using System.IO;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary> 
/// 資料庫存取類別
/// </summary> 
public class dbFunction
{
    #region - 宣告 -

    private string strConn;
    private SqlConnection Conn;
    logFunction logFun = new logFunction();

    #endregion

    public dbFunction()
    {
        //正式機
        this.strConn = @"Data Source=10.13.210.33;Initial Catalog=MAM;User ID=mam_admin;Password=ptsP@ssw0rd";
        //測試機
        //this.strConn = @"Data Source=10.13.220.35\MSSQL2008;Initial Catalog=MAM;User ID=mam_admin;Password=ptsP@ssw0rd";

        this.Conn = new SqlConnection(this.strConn);
    }

    /// <summary>
    /// 執行不回傳的T-SQL
    /// </summary>
    /// <param name="strSQL">T-SQL字串</param>
    public void EditData(string strSQL)
    {
        using (SqlConnection Conn = new SqlConnection(strConn))
        {
            using (SqlCommand Cmd = new SqlCommand(strSQL, Conn))
            {
                try
                {
                    Conn.Open();
                    Cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    logFun.LogError("MAM_PGM_QC.dbFunction()", String.Format("錯誤：{0} 語法：{1}", ex.ToString(), strSQL));
                }
            }
        }
    }

    /// <summary>
    /// 執行T-SQL，結果回傳至DataTable
    /// </summary>
    /// <param name="strSQL">T-SQL字串</param>
    public DataTable CreateDataTable(string strSQL) //多型
    {
        using (SqlConnection Conn = new SqlConnection(strConn))
        {
            using (SqlCommand Cmd = new SqlCommand(strSQL, Conn))
            {
                DataSet ds = new DataSet();

                try
                {
                    Conn.Open();
                    SqlDataAdapter da = new SqlDataAdapter(strSQL, this.Conn);
                    da.Fill(ds, "TableName");
                }
                catch (Exception ex)
                {
                    logFun.LogError("MAM_PGM_QC.dbFunction()", String.Format("錯誤：{0} 語法：{1}", ex.ToString(), strSQL));
                }

                return ds.Tables["TableName"];
            }
        }
    }

    /// <summary>
    /// 執行T-SQL，結果回傳至DataTable，適用於DropDownList
    /// </summary>
    /// <param name="strSQL">T-SQL字串</param>
    public DataTable DataTableToDropDownList(string strSQL)
    {
        DataTable dt = CreateDataTable(strSQL);
        DataRow dr = dt.NewRow();
        dr["TEXT"] = " - 請選擇 - ";
        dr["VALUE"] = "";
        dt.Rows.InsertAt(dr, 0);

        return dt;
    }
}