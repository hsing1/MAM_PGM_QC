//---------------------------------------------------------------------------
// MLProtect_MPlatformSDK.cs : Personal protection code for the MediaLooks License system
//---------------------------------------------------------------------------
// Copyright (c) 2012, MediaLooks Soft
// www.medialooks.com (dev@medialooks.com)
//
// Authors: MediaLooks Team
// Version: 3.0.0.0
//
//---------------------------------------------------------------------------
// CONFIDENTIAL INFORMATION
//
// This file is Intellectual Property (IP) of MediaLooks Soft and is
// strictly confidential. You can gain access to this file only if you
// sign a License Agreement and a Non-Disclosure Agreement (NDA) with
// MediaLooks Soft If you had not signed any of these documents, please
// contact <dev@medialooks.com> immediately.
//
//---------------------------------------------------------------------------
// Usage:
//
// 1. Copy MLProxy.dll from the license package to any folder and register it
//    in the system registry by using regsvr32.exe utility
//
// 2. Add the reference to MLProxy.dll in the C# project
//
// 3. Add this file to the project
//
// 4. Call MPlatformSDKLic.IntializeProtection() method before creating any Medialooks 
//    objects (for e.g. in Form_Load event handler)
//
// 5. Compile the project
//
// IMPORTANT: If your application is running for a period longer than 24 
//            hours, the IntializeProtection() call should be repeated 
//            periodically  (every 24 hours or less).
//
// IMPORTANT: If your have several Medialooks products, don't forget to initialize
//            protection for all of them. For e.g.
//
//             MPlatformLic.IntializeProtection();
//             MDecodersLic.IntializeProtection();
//             etc.

using System;

    public class MPlatformSDKLic
    {        
        private static MLPROXYLib.CoMLProxyClass m_objMLProxy;
        private static string strLicInfo = @"[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={325A978E-AEDD-416C-93E0-7C02142F4DDC}
License.Key={B9974171-18AF-492D-C3F1-38AA80AD89FD}
License.Name=MPlaylist Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=A5ED81A31A411C4D3288AEC8BF7413457FC6B9694B4DE4BD5C2FCE8FDA52974C4D419969D963CFACCCE8D4BF514F2A80645D21E0D36758DC899B8D50B88307C8C0C9D48B741BB37823F8FEB04476053EDF2697BC81D9EEA603224E122D85EFF615B7EE09354C7F0F0A5C95F6B0CB950EBE318A46C250C10E9D4C6B0CBF2E8DA7

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={17726C27-E8AE-4823-92E6-2AA58C3C4D53}
License.Key={FFA1A624-ACEE-5D39-1E36-27C70EFFC1FE}
License.Name=MFile Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=A92C8527465A7878B61AD148C57FAFD708CA005DEA15DB3D7F194DBB0D6F7E35AB71A68E2051C06BB310C26981B747605097EF4903F15BB429DE42D9984F457D6177AD13745637F3553A60579710AAB89B0E26CA78D9FB229A0677C3E0FA810FF2E74DC14579FCC9F0E94816A2EB5B37F518256B130A963B98C4662F12033E29

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={5E876E15-8741-411A-9B05-32858A79B151}
License.Key={0FD81A88-59B7-6613-EE6F-9F1AD37790DF}
License.Name=MLive Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=8DFF66BD3A43C38867DE962CD9175DB11C4989C7F975AED20B5748B72C511AD49A89A371509FA738F136D9BCC1EB381BC94F4A29B0092B4E530C3DF50848D7CCF9435B567335615F617315FA610942F5813C182C135970457593FB4BA0597FD470D60FCB83AD59F733B216CF1F4EDC3ABC0F7157D0E68BA360A709266F470374

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={AA1F7511-473E-4C98-AEDF-91CCAFD1E96D}
License.Key={0B4EDC6E-FED3-DBF7-0643-C609E14662B2}
License.Name=MRenderer Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=1D7A2A3D2B59DFC5C9CC6793ED5127B2DD7B09EF400F17313B45BD050636B745CAD99831D14D8640EC216DFD73C75F15DF5472026E183AA048B320464B90BE5A19644C1C8F46CC4EB50FF6E43C0CCC155BF0A4EA5E2CED163F0A93F686F886B5C7A69AF88695A858114E8A8CC7EACCA10871BBAB5DF34DF4F0600BADCB223AB7

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={5200A20F-81C3-4742-B12E-6C53CA3FBF9A}
License.Key={88008F2B-1D7B-8342-5393-4C73115C3C57}
License.Name=MTransportDS Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=2F0C418EBC5AC3EAB495D47CA104A4F9DAAFD820472118BF7213DF472476E335873B83502F068A8FA0B979412A298DBCD6CB310845F38861655697B7457F26A9748C044C1AE59BD34100A3BE83D6645E71F44A10A2D16987B5A09AF7CCE1B83265DAD07D6514F8E16C052E471D50AC9B27C252393A0D0B3D6E60CF1B3F39DB68

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={13086C7B-1FA7-4886-B0C6-FA7C486FAE7B}
License.Key={6612BD5D-B1F6-48F4-3041-B04634CB2A2F}
License.Name=MMixer Module (BETA)
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=EC09E78E589DEF6B45216423ACD6108C3E1DC714E91C554DC77A0D724A4F1C0F7C96C80038F719CC47038FD648CD2A2A822A05887E089ED1B9CF4CBA5630D4BE541D02DA791EC7243FA2C8626C6E0DA88BF5057B6C00701A3E76BD0620A312E6271188BD4D032127C3E93DB7769BA409D76D9EDFC3E917479BD6B203CF64B64E

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={62D0E1C9-A425-46B9-B19C-27A4DED73417}
License.Key={0B4EDC6E-FDA0-3143-823C-6D375B559EAD}
License.Name=MediaLooks Character Generator
License.UpdateExpirationDate=September 1, 2016
License.Edition=Professional
License.AllowedModule=*.*
License.Signature=58981ABE0AD1F2F45C84D47156009DD1866818BC4958C8482BD4A3D0DDA97027BCEE9A31BE68F2DB8D104C7F3EC899C38FBD45BAFA41AAB3AC664DBF51FAFD79445E1C61791D7615FBE0045CBE22E61641A6E5CD48A5705EDC9AD23BBE9115036A4875E86B98DFE4908DADA9934B964A13138325BE4C712E289B6413F9C5F553

[MediaLooks]
License.ProductName=MPlatform SDK
License.IssuedTo=Formosa Television Co.Ltd.
License.CompanyID=144
License.UUID={B89683C3-C81D-4AB0-A150-CC97BF363046}
License.Key={7D793181-D651-059A-33E4-C7747C34D39C}
License.Name=MPlatform Module
License.UpdateExpirationDate=September 1, 2016
License.Edition=Standard
License.AllowedModule=*.*
License.Signature=D7F3ED2D31F2837A9AEAFDE251C963BE932703CAC400A0214F040EEC986401F4B85DC9A832E612EA3CA05F708C36801CA021A2AD228F99E13FD98030C6F91A57966B1BC370F00CF4C03C3163C1477DDB7E8EEE04EA41B89A81D431D272AF9C5A0B9539C2D21E1A867748D8F1D6F05297AF0A71FB3909E76489ABE3E1C23E8C89

";

		//License initialization
        public static void IntializeProtection()
        {
            if (m_objMLProxy == null)
            {
                // Create MLProxy object 
                m_objMLProxy = new MLPROXYLib.CoMLProxyClass();
                m_objMLProxy.PutString(strLicInfo);                
            }
           UpdatePersonalProtection();
        }

        private static void UpdatePersonalProtection()
        {
            ////////////////////////////////////////////////////////////////////////
            // MediaLooks License secret key
            // Issued to: Formosa Television Co.Ltd.
            const long _Q1_ = 47742703;
            const long _P1_ = 51237101;
            const long _Q2_ = 51379201;
            const long _P2_ = 46832743;

            try
            {

                int nFirst = 0;
                int nSecond = 0;
                m_objMLProxy.GetData(out nFirst, out  nSecond);

                // Calculate First * Q1 mod P1
                long llFirst = (long)nFirst * _Q1_ % _P1_;
                // Calculate Second * Q2 mod P2
                long llSecond = (long)nSecond * _Q2_ % _P2_;

                uint uRes = SummBits((uint)(llFirst + llSecond));

                // Calculate check value
                long llCheck = (long)(nFirst - 29) * (nFirst - 23) % nSecond;
                // Calculate return value
                int nRand = new Random().Next(0x7FFF);
                int nValue = (int)llCheck + (int)nRand * (uRes > 0 ? 1 : -1);

                m_objMLProxy.SetData(nFirst, nSecond, (int)llCheck, nValue);

            }
            catch (System.Exception) { }

        }

        private static uint SummBits(uint _nValue)
        {
            uint nRes = 0;
            while (_nValue > 0)
            {
                nRes += (_nValue & 1);
                _nValue >>= 1;
            }

            return nRes % 2;
        }
    }