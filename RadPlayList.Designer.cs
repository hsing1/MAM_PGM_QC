﻿namespace MAM_PGM_QC
{
    partial class RadPlayList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn1 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn2 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.ConditionalFormattingObject conditionalFormattingObject1 = new Telerik.WinControls.UI.ConditionalFormattingObject();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn3 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn4 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn5 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn6 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn7 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn8 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn9 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn10 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn11 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn12 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn13 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn14 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn15 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn16 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.GridViewTextBoxColumn gridViewTextBoxColumn17 = new Telerik.WinControls.UI.GridViewTextBoxColumn();
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RadPlayList));
            this.radGvPlayList = new Telerik.WinControls.UI.RadGridView();
            ((System.ComponentModel.ISupportInitialize)(this.radGvPlayList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGvPlayList.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGvPlayList
            // 
            this.radGvPlayList.AllowDrop = true;
            this.radGvPlayList.BackColor = System.Drawing.SystemColors.ControlDark;
            this.radGvPlayList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radGvPlayList.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radGvPlayList.Location = new System.Drawing.Point(0, 0);
            // 
            // 
            // 
            this.radGvPlayList.MasterTemplate.AllowAddNewRow = false;
            this.radGvPlayList.MasterTemplate.AllowColumnReorder = false;
            gridViewTextBoxColumn1.FieldName = "FNCONBINE_NO";
            gridViewTextBoxColumn1.HeaderText = "序號";
            gridViewTextBoxColumn1.Name = "FNCONBINE_NO";
            gridViewTextBoxColumn1.Width = 40;
            conditionalFormattingObject1.CellBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            conditionalFormattingObject1.CellFont = new System.Drawing.Font("新細明體", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            conditionalFormattingObject1.CellForeColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.Name = "NewCondition";
            conditionalFormattingObject1.RowBackColor = System.Drawing.Color.Empty;
            conditionalFormattingObject1.RowForeColor = System.Drawing.Color.Empty;
            gridViewTextBoxColumn2.ConditionalFormattingObjectList.Add(conditionalFormattingObject1);
            gridViewTextBoxColumn2.FieldName = "FSPLAY_TIME";
            gridViewTextBoxColumn2.HeaderText = "播出時間";
            gridViewTextBoxColumn2.Name = "FSPLAY_TIME";
            gridViewTextBoxColumn2.Width = 80;
            gridViewTextBoxColumn3.FieldName = "FSPROG_ID";
            gridViewTextBoxColumn3.HeaderText = "節目 / 短帶編號";
            gridViewTextBoxColumn3.Name = "FSPROG_ID";
            gridViewTextBoxColumn3.Width = 95;
            gridViewTextBoxColumn4.FieldName = "FSPROG_NAME";
            gridViewTextBoxColumn4.HeaderText = "節目 / 短帶名稱";
            gridViewTextBoxColumn4.Name = "FSPROG_NAME";
            gridViewTextBoxColumn4.Width = 230;
            gridViewTextBoxColumn5.FieldName = "FNEPISODE";
            gridViewTextBoxColumn5.HeaderText = "集數";
            gridViewTextBoxColumn5.Name = "FNEPISODE";
            gridViewTextBoxColumn5.Width = 40;
            gridViewTextBoxColumn6.FieldName = "FNBREAK_NO";
            gridViewTextBoxColumn6.HeaderText = "支數";
            gridViewTextBoxColumn6.Name = "FNBREAK_NO";
            gridViewTextBoxColumn6.Width = 40;
            gridViewTextBoxColumn7.FieldName = "FCPROPERTY";
            gridViewTextBoxColumn7.HeaderText = "短帶/節目";
            gridViewTextBoxColumn7.Name = "FCPROPERTY";
            gridViewTextBoxColumn7.Width = 70;
            gridViewTextBoxColumn8.FieldName = "FSBEG_TIMECODE";
            gridViewTextBoxColumn8.HeaderText = "IN 點";
            gridViewTextBoxColumn8.Name = "FSBEG_TIMECODE";
            gridViewTextBoxColumn8.Width = 80;
            gridViewTextBoxColumn9.FieldName = "FSEND_TIMECODE";
            gridViewTextBoxColumn9.HeaderText = "OUT點";
            gridViewTextBoxColumn9.Name = "FSEND_TIMECODE";
            gridViewTextBoxColumn9.Width = 80;
            gridViewTextBoxColumn10.FieldName = "FSDUR";
            gridViewTextBoxColumn10.HeaderText = "秒數";
            gridViewTextBoxColumn10.Name = "FSDUR";
            gridViewTextBoxColumn10.Width = 80;
            gridViewTextBoxColumn11.FieldName = "FSARC_TYPE";
            gridViewTextBoxColumn11.HeaderText = "解析度";
            gridViewTextBoxColumn11.Name = "FSARC_TYPE";
            gridViewTextBoxColumn11.Width = 55;
            gridViewTextBoxColumn12.FieldName = "FSFILE_NO";
            gridViewTextBoxColumn12.HeaderText = "檔案編號";
            gridViewTextBoxColumn12.Name = "FSFILE_NO";
            gridViewTextBoxColumn12.Width = 130;
            gridViewTextBoxColumn13.FieldName = "FSVIDEO_ID";
            gridViewTextBoxColumn13.HeaderText = "影片編號";
            gridViewTextBoxColumn13.Name = "FSVIDEO_ID";
            gridViewTextBoxColumn13.Width = 80;
            gridViewTextBoxColumn14.FieldName = "FSMEMO";
            gridViewTextBoxColumn14.HeaderText = "備註";
            gridViewTextBoxColumn14.Name = "FSMEMO";
            gridViewTextBoxColumn14.Width = 120;
            gridViewTextBoxColumn15.FieldName = "FNLIVE";
            gridViewTextBoxColumn15.HeaderText = "LIVE";
            gridViewTextBoxColumn15.Name = "FNLIVE";
            gridViewTextBoxColumn16.FieldName = "FCFILE_STATUS";
            gridViewTextBoxColumn16.HeaderText = "檔案狀態";
            gridViewTextBoxColumn16.Name = "FCFILE_STATUS";
            gridViewTextBoxColumn16.Width = 80;
            gridViewTextBoxColumn17.FieldName = "FCSTATUS";
            gridViewTextBoxColumn17.HeaderText = "HD狀態";
            gridViewTextBoxColumn17.Name = "FCSTATUS";
            gridViewTextBoxColumn17.Width = 80;
            this.radGvPlayList.MasterTemplate.Columns.AddRange(new Telerik.WinControls.UI.GridViewDataColumn[] {
            gridViewTextBoxColumn1,
            gridViewTextBoxColumn2,
            gridViewTextBoxColumn3,
            gridViewTextBoxColumn4,
            gridViewTextBoxColumn5,
            gridViewTextBoxColumn6,
            gridViewTextBoxColumn7,
            gridViewTextBoxColumn8,
            gridViewTextBoxColumn9,
            gridViewTextBoxColumn10,
            gridViewTextBoxColumn11,
            gridViewTextBoxColumn12,
            gridViewTextBoxColumn13,
            gridViewTextBoxColumn14,
            gridViewTextBoxColumn15,
            gridViewTextBoxColumn16,
            gridViewTextBoxColumn17});
            this.radGvPlayList.MasterTemplate.ShowFilteringRow = false;
            this.radGvPlayList.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGvPlayList.Name = "radGvPlayList";
            this.radGvPlayList.ReadOnly = true;
            this.radGvPlayList.Size = new System.Drawing.Size(1453, 881);
            this.radGvPlayList.TabIndex = 0;
            this.radGvPlayList.RowFormatting += new Telerik.WinControls.UI.RowFormattingEventHandler(this.radGvPlayList_RowFormatting);
            this.radGvPlayList.CellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.radGvPlayList_CellFormatting);
            this.radGvPlayList.CellDoubleClick += new Telerik.WinControls.UI.GridViewCellEventHandler(this.radGvPlayList_CellDoubleClick);
            // 
            // RadPlayList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1453, 881);
            this.Controls.Add(this.radGvPlayList);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "RadPlayList";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "公共電視台 影片檔案 Quality Control 應用程式";
            this.Load += new System.EventHandler(this.RadPlayList_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGvPlayList.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGvPlayList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.UI.RadGridView radGvPlayList;
    }
}
