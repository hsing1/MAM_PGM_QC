﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Telerik.WinControls;
using MAM_PGM_QC.DataStruct;
using MAM_PGM_QC.App_Code;
using MPLATFORMLib;
using MControls;
using System.Diagnostics;

namespace MAM_PGM_QC
{
    /// <summary>
    /// 公共電視 數位檔案 QC 軟體
    /// </summary>
    public partial class RadMainForm : Telerik.WinControls.UI.RadForm
    {
        #region - 宣告 -

        MPlaylistClass m_objPlaylist;
        PlayListBo playlistBo = new PlayListBo();
        QCResultBo resultBo = new QCResultBo();
        OptionItemsBo itemBo = new OptionItemsBo();

        #endregion

        #region - 初始化 -

        public RadMainForm()
        {
            InitializeComponent();
        }

        private void RadMainForm_Load(object sender, EventArgs e)
        {
            MPlatformSDKLic.IntializeProtection();
            EncoderlibLic.IntializeProtection();
            DecoderlibLic.IntializeProtection();

            SetDataToForm();
        }

        private void RadMainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (m_objPlaylist != null)
            {
                mPreviewControl1.SetControlledObject(null);
                m_objPlaylist.ObjectClose();
            }
        }

        void m_objPlaylist_OnEvent(string bsChannelID, string bsEventName, string bsEventParam, object pEventObject)
        {
            if (bsEventName == "EOF")  //跑完播表後執行
            {
                m_objPlaylist.FilePlayPause(0);
            }

            mFileStateControl2.UpdateState();

            UpdateSeekControl();
        }

        void m_objPlaylist_OnFrame(string bsChannelID, object pMFrame)
        {
            mSeekControl2.UpdatePos();
        }

        void UpdateSeekControl()
        {
            try
            {
                m_objPlaylist.PlaylistGetByIndex(-1, out offset, out path, out pFile);
                mSeekControl2.SetControlledObject(pFile);
            }
            catch
            {
            }
        }

        void mAudioMeter1_SizeChanged(object sender, EventArgs e)
        {
            if (this.Width > 180)
            {
                mPreviewControl1.Width = this.Width - 28 - mAudioMeter1.Bounds.Right;
                mPreviewControl1.Height = mAudioMeter1.Height;
                mPreviewControl1.Location = new Point(mAudioMeter1.Bounds.Right + 6, mPreviewControl1.Location.Y);
            }

            Trace.WriteLine(mPreviewControl1.Bounds);
        }

        #endregion

        #region - 方法 -

        /// <summary>
        /// 資料 To 視窗畫面
        /// </summary>
        private void SetDataToForm()
        {
            this.Text = String.Format("公共電視台 影片檔案 Quality Control 應用程式版（{0}）", GlobalParameters.AppVersion);

            m_objPlaylist = new MPlaylistClass();
            m_objPlaylist.PropsSet("loop", "true");
            m_objPlaylist.OnFrame += new IMEvents_OnFrameEventHandler(m_objPlaylist_OnFrame);
            m_objPlaylist.OnEvent += new IMEvents_OnEventEventHandler(m_objPlaylist_OnEvent);
            m_objPlaylist.ObjectStart(null);
            mPreviewControl1.SetControlledObject(m_objPlaylist);
            //mFileStateControl1.SetControlledObject(m_objPlaylist);
            mFileStateControl2.SetControlledObject(m_objPlaylist);
            mFileSpec1.SetControlledObject(m_objPlaylist);
            //mRateControl1.SetControlledObject(m_objPlaylist);
            mSeekControl2.SetControlledObject(m_objPlaylist);

            mAudioMeter1.SetControlledObject(m_objPlaylist);
            mAudioMeter1.SizeChanged += new EventHandler(mAudioMeter1_SizeChanged);
            mAudioMeter1_SizeChanged(null, null);

            radDrpFSCHANNEL_ID.DataSource = itemBo.SelectChannelItem();
            radDrpFSCHANNEL_ID.DisplayMember = "TEXT";
            radDrpFSCHANNEL_ID.ValueMember = "VALUE";
            radDrpFSCHANNEL_ID.SelectedIndex = 1;

            GetData();
        }

        /// <summary>
        /// 查詢條件 驗證
        /// </summary>
        private bool QueryVerify()
        {
            bool IsVerify = true;
            _ErrMsg = String.Empty;

            if (String.IsNullOrEmpty(radDtpFDDATE.Text))
            {
                _ErrMsg += "請選擇「播出日期」\n";
                IsVerify = false;
            }

            if (String.IsNullOrEmpty(radDrpFSCHANNEL_ID.SelectedValue.ToString()))
            {
                _ErrMsg += "請選擇「播出頻道」";
                IsVerify = false;
            }

            return IsVerify;
        }

        #region TimeCode 相關

        /// <summary>
        /// 至 IN 點
        /// </summary>
        private void SetFilePosTCIN()
        {
            DataGridView dgv = dgvPlayList;

            if (dgv.CurrentRow.Selected)
            {
                _FilePath = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                _TCIn = dgv.CurrentRow.Cells["FSBEG_TIMECODE"].Value.ToString().Trim();
                _TCOut = dgv.CurrentRow.Cells["FSEND_TIMECODE"].Value.ToString().Trim();
                _TCPos = _TCIn;

                if (File.Exists(_FilePath))
                {
                    SetFilePosTC();
                }
                else
                {
                    MessageBox.Show("檔案不存在！");
                }
            }
        }

        /// <summary>
        /// 至 OUT 點
        /// </summary>
        private void SetFilePosTCOUT()
        {
            DataGridView dgv = dgvPlayList;

            if (dgv.CurrentRow.Selected)
            {
                _FilePath = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                _TCIn = dgv.CurrentRow.Cells["FSBEG_TIMECODE"].Value.ToString().Trim();
                _TCOut = dgv.CurrentRow.Cells["FSEND_TIMECODE"].Value.ToString().Trim();
                _TCPos = _TCOut;

                if (File.Exists(_FilePath))
                {
                    SetFilePosTC();
                }
                else
                {
                    MessageBox.Show("檔案不存在！");
                }
            }
        }

        /// <summary>
        /// 設定 TimeCode 至 Pos 點 
        /// </summary>
        private void SetFilePosTC()
        {
            try
            {
                #region 加入至 Playlist

                PlaylistRemove();

                int nIndex = -1;
                //int OutSpecified;

                m_objPlaylist.PlaylistAdd(null, _FilePath, "", ref nIndex, out pFile);

                #endregion

                #region 設定 TimeCode 點

                if (pFile != null)
                {
                    //((IMFile)pFile).FileInOutGetTC(out pTCIn, out pTCOut, out OutSpecified);
                    ((IMFile)pFile).FilePosGetTC(out pTCPos);

                    //bool validNewTCIn = StrToTC(_TCIn, ref pTCIn);
                    //bool validNewTCOut = StrToTC(_TCOut, ref pTCOut);
                    bool validNewTCPos = StrToTC(_TCPos, ref pTCPos);

                    //((IMFile)pFile).FileInOutSetTC(pTCIn, pTCOut);
                    ((IMFile)pFile).FilePosSetTC(pTCPos);
                    ((IMFile)pFile).FilePlayStart();
                    ((IMFile)m_objPlaylist).FilePlayPause(0);

                    tcBoxPos.Text = _TCPos;
                }

                if (pFile == null) return;

                System.Runtime.InteropServices.Marshal.ReleaseComObject(pFile);
                GC.Collect();

                #endregion
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// TimeCode 字串 至 M_TIMECODE
        /// </summary>
        /// <param name="strTC">TimeCode字串</param>
        /// <param name="tc">M_TIMECODE</param>
        private bool StrToTC(string strTC, ref M_TIMECODE tc)
        {
            bool res = false;
            string[] strArray = strTC.Split(':');

            if (strArray.Length == 4)
            {
                int nHours, nMins, nSecs, nFrames;

                bool hoursValid = Int32.TryParse(strArray[0], out nHours);
                bool minsValid = Int32.TryParse(strArray[1], out nMins);
                bool secsValid = Int32.TryParse(strArray[2], out nSecs);
                bool framesValid = Int32.TryParse(strArray[3], out nFrames);

                if (hoursValid && minsValid && secsValid && framesValid &&
                    nHours >= 0 && nHours <= 24 &&
                    nMins >= 0 && nMins <= 60 &&
                    nSecs >= 0 && nSecs <= 60 &&
                    nFrames >= 0 && nFrames <= 35)
                {
                    tc.nHours = nHours;
                    tc.nMinutes = nMins;
                    tc.nSeconds = nSecs;
                    tc.nFrames = nFrames;
                    res = true;
                }
            }

            return res;
        }

        /// <summary>
        /// 刪除 Playlist
        /// </summary>
        private void PlaylistRemove()
        {
            int nFiles = 0;
            double dblDuration = 0;
            m_objPlaylist.PlaylistGetCount(out nFiles, out dblDuration);

            if (nFiles > 0)
            {
                m_objPlaylist.PlaylistRemoveByIndex(0, 0);
            }
        }

        #region - 手動輸入 TimeCode -

        /// <summary>
        /// 輸入TimeCode 至 Pos定點
        /// </summary>
        private void btnGoPos_Click(object sender, EventArgs e)
        {
            try
            {
                DataGridView dgv = dgvPlayList;

                if (dgv.CurrentRow.Selected)
                {
                    _FilePath = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                    _TCIn = dgv.CurrentRow.Cells["FSBEG_TIMECODE"].Value.ToString().Trim();
                    _TCOut = dgv.CurrentRow.Cells["FSEND_TIMECODE"].Value.ToString().Trim();
                    _TCPos = tcBoxPos.Text.Replace(".", ":");

                    SetFilePosTC();

                    //if (File.Exists(_FilePath))
                    //{
                    //    DateTime dtTC = new TimeCode(_TCPos).ToTime();
                    //    DateTime dtIN = new TimeCode(_TCIn).ToTime();
                    //    DateTime dtOUT = new TimeCode(_TCOut).ToTime();

                    //    if (DateTime.Compare(dtIN, dtTC) <= 0)
                    //    {
                    //        if (DateTime.Compare(dtTC, dtOUT) <= 0)
                    //        {
                    //            SetFilePosTC();
                    //        }
                    //        else
                    //        {
                    //            MessageBox.Show("時間大於 OUT 點");
                    //        }
                    //    }
                    //    else
                    //    {
                    //        MessageBox.Show("時間小於 IN 點");
                    //    }
                    //}
                }
            }
            catch (System.Exception) { }
        }

        /// <summary>
        /// 輸入TimeCode完畢後，按Enter執行
        /// </summary>
        private void tcBoxPos_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnGoPos_Click(this, null);
            }
        }

        /// <summary>
        /// 至 IN 點
        /// </summary>
        private void btnGoTCIn_Click(object sender, EventArgs e)
        {
            SetFilePosTCIN();
        }

        /// <summary>
        /// 至 OUT 點
        /// </summary>
        private void btnGoTCOut_Click(object sender, EventArgs e)
        {
            SetFilePosTCOUT();
        }

        #endregion

        #endregion

        #endregion

        #region - 事件 -

        #region - DataGridView -

        /// <summary>
        /// 載入QC表單
        /// </summary>
        public void GetData()
        {
            if (String.IsNullOrEmpty(radDtpFDDATE.Text))
            {
                radDtpFDDATE.Text = DateTime.Now.ToString("yyyy/MM/dd");
            }

            playlistBo.vo.FSCHANNEL_ID = radDrpFSCHANNEL_ID.SelectedValue.ToString();
            playlistBo.vo.FDDATE = radDtpFDDATE.Text;

            DataTable dt = playlistBo.GetPlayListForQC();
            Hashtable htVIDEO_HDMC = GlobalMsgTxt.CodeToMsg("VIDEO_HDMC");

            dgvPlayList.Rows.Clear();

            DataTable dtWaitQC = GlobalParameters.GetFilePathList(GlobalParameters.WaitQCFilePath);
            DataTable dtFilesList = GlobalParameters.GetFilePathList(GlobalParameters.DoneQCFilePath);
            dtFilesList.Merge(dtWaitQC);
          
            foreach (DataRow dr in dt.Rows)
            {
                bool IsExist = false;

                string IdByFilePath = String.Empty;

                foreach (DataRow drFiles in dtFilesList.Rows)
                {
                    string IdByDB = dr["FSVIDEO_ID"].ToString();
                    string IdByFile = drFiles["fileName"].ToString();

                    if (IdByDB.Equals(IdByFile))
                    {
                        IsExist = true;
                        IdByFilePath = drFiles["filePath"].ToString();    //審核通過，需看兩邊路徑
                    }
                }

                bool IsUseQC = false;

                if ("BYT".IndexOf(dr["FCFILE_STATUS"].ToString()) >= 0)
                {
                     if (dr["FCSTATUS"].ToString().Equals("N"))
                    {
                        if (dr["FSSEG_STATUS"].ToString().Equals("1"))
                        {
                            IsUseQC = true;
                        }
                    }
                }

                dgvPlayList.Rows.Add(
                    dgvPlayList.Rows.Count + 1,
                    dr["FSVIDEO_ID"].ToString(),
                    dr["FSPROG_NAME"].ToString(),
                    dr["FNEPISODE"].ToString(),
                    dr["FSBEG_TIMECODE"].ToString().Replace(";",":"),
                    dr["FSEND_TIMECODE"].ToString().Replace(";", ":"),
                    dr["FSARC_TYPE"].ToString(),
                    dr["FCFILE_STATUS"].ToString(),
                    htVIDEO_HDMC[dr["FCSTATUS"].ToString()],
                    IsExist,
                    (dr["FSSEG_STATUS"].ToString().Equals("1")) ? true : false,
                    IsUseQC,
                    IdByFilePath,
                    dr["FSFILE_PATH_L"].ToString(),
                    dr["FSFILE_NO"].ToString(),
                    dr["FSPROG_ID"].ToString()
                );
            }
        }

        /// <summary>
        /// 載入QC表單(由日播表定位至QC播表)
        /// </summary>
        public void GetData(string VideoID, string TC_IN, string TC_OUT)
        {
            for (int i = 0; i < dgvPlayList.Rows.Count; i++)
            {
                if (dgvPlayList.Rows[i].Cells["FSVIDEO_ID"].Value.ToString().Equals(VideoID))
                {
                    if (dgvPlayList.Rows[i].Cells["FSBEG_TIMECODE"].Value.ToString().Equals(TC_IN))
                    {
                        if (dgvPlayList.Rows[i].Cells["FSEND_TIMECODE"].Value.ToString().Replace(";", ":").Equals(TC_OUT))
                        {
                             _RowIndex = i;
                        }
                    }
                }
            }

            dgvPlayList.CurrentCell = dgvPlayList.Rows[_RowIndex].Cells[0];
        }

        /// <summary>
        /// 載入QC表單
        /// </summary>
        public void GetDataReload()
        {
            if (dgvPlayList.Rows.Count > 0)
            {
                _FirstRowIndex = dgvPlayList.FirstDisplayedScrollingRowIndex;
                _RowIndex = dgvPlayList.CurrentRow.Index;

                GetData();

                if (_FirstRowIndex != 0)
                {
                    dgvPlayList.FirstDisplayedScrollingRowIndex = _FirstRowIndex;
                    dgvPlayList.CurrentCell = dgvPlayList.Rows[_RowIndex].Cells[0];
                }
            }
        }     

        /// <summary>
        /// 雙擊點選至 IN 點
        /// </summary>
        private void dgvPlayList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (dgv == null)
            {
                return;
            }

            if (dgv.CurrentRow.Selected)
            {
                if (e.RowIndex > 0)
                {
                    bool IsExist = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_EXIST"].Value.ToString());
                    bool IsUseQC = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_USEQC"].Value.ToString());

                    menuItemShow(IsExist, IsUseQC);

                    if (IsExist)
                    {
                        _FilePath = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                        _TCIn = dgv.CurrentRow.Cells["FSBEG_TIMECODE"].Value.ToString().Trim();
                        _TCOut = dgv.CurrentRow.Cells["FSEND_TIMECODE"].Value.ToString().Trim();
                        _TCPos = _TCIn;

                        if (File.Exists(_FilePath))
                        {
                            SetFilePosTC();
                        }
                    }
                    else
                    {
                        MessageBox.Show("檔案不存在！");
                    }
                }
            }
        }

        /// <summary>
        /// 右鍵選單 防呆 (顯示/隱藏)
        /// </summary>
        private void dgvPlayList_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (dgv == null)
            {
                return;
            }

            if (dgv.CurrentRow.Selected)
            {
                if (e.RowIndex > 0)
                {
                    bool IsExist = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_EXIST"].Value.ToString());
                    bool IsUseQC = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_USEQC"].Value.ToString());

                    menuItemShow(IsExist, IsUseQC);
                }
            }
        }

        /// <summary>
        /// 檔案狀態 底色
        /// </summary>
        private void dgvPlayList_RowPrePaint(object sender, DataGridViewRowPrePaintEventArgs e)
        {
            DataGridView dgv = sender as DataGridView;

            if (dgv == null)
            {
                return;
            }

            bool IsExist = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_EXIST"].Value.ToString());
            bool IsUseQC = bool.Parse(dgv.Rows[e.RowIndex].Cells["FSFILE_USEQC"].Value.ToString());

            menuItemShow(IsExist, IsUseQC);

            if (dgv.Rows.Count <= e.RowIndex) return;

            if (e.RowIndex < 0) return;

            //if (IsExist)
            //{
                if (dgv.Rows[e.RowIndex].Cells["FCSTATUS"].Value.Equals(GlobalMsgTxt.strOK))
                {
                    dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.PaleGreen;
                }
                else if (dgv.Rows[e.RowIndex].Cells["FCSTATUS"].Value.Equals(GlobalMsgTxt.strNG))
                {
                    dgv.Rows[e.RowIndex].DefaultCellStyle.BackColor = Color.Thistle;
                }
            //}
        }

        #region 右鍵選單

        /// <summary>
        /// 右鍵選單 (項目顯示或隱藏)
        /// </summary>
        /// <param name="IsExist">檔案是否存在</param>
        /// <param name="IsUseQC">檔案是否可以QC</param>
        private void menuItemShow(bool IsExist, bool IsUseQC) 
        {
            //contextMenuStrip1.Enabled = false;

            menuItemTCIN.Visible = false;
            menuItemTCOUT.Visible = false;
            menuItemQC.Visible = true;
                menuItemQCResultOK.Visible = false;
                menuItemQCResultNG.Visible = false;
                menuItemQCResultNote.Visible = true;
            menuItemLine1.Visible = true;
            menuItemCopyVideoID.Visible = true;

            if (IsExist)
            {
                menuItemTCIN.Visible = true;
                menuItemTCOUT.Visible = true;

                if (IsUseQC)
                {
                    menuItemQCResultOK.Visible = true;
                    menuItemQCResultNG.Visible = true;
                }
                else
                {
                    menuItemQCResultOK.Visible = false;
                    menuItemQCResultNG.Visible = false;
                }
            }
        }

        /// <summary>
        /// 至 IN 點
        /// </summary>
        private void menuItemTCIN_Click(object sender, EventArgs e)
        {
            SetFilePosTCIN();
        }

        /// <summary>
        /// 至 OUT 點
        /// </summary>
        private void menuItemTCOUT_Click(object sender, EventArgs e)
        {
            SetFilePosTCOUT();
        }

        /// <summary>
        /// QC 檢查結果：通過
        /// </summary>
        private void menuItemQCResultOK_Click(object sender, EventArgs e)
        {
            DataGridView dgv = dgvPlayList;

            if (dgv.CurrentRow.Selected)
            {
                if (!resultBo.GetQCFileIsChecked(dgv.CurrentRow.Cells["FSVIDEO_ID"].Value.ToString().Trim()))
                {
                    //新增 TBLOG_QC_RESULT
                    resultBo.vo.FSVIDEO_ID = dgv.CurrentRow.Cells["FSVIDEO_ID"].Value.ToString().Trim();
                    resultBo.vo.FSFILE_NO = dgv.CurrentRow.Cells["FSFILE_NO"].Value.ToString().Trim();
                    resultBo.vo.FSPROG_ID = dgv.CurrentRow.Cells["FSPROG_ID"].Value.ToString().Trim();
                    resultBo.vo.FSPROG_NAME = dgv.CurrentRow.Cells["FSPROG_NAME"].Value.ToString().Trim();
                    resultBo.vo.FNEPISODE = int.Parse(dgv.CurrentRow.Cells["FNEPISODE"].Value.ToString().Trim());
                    resultBo.vo.FSQC_DESCRIPTION = GlobalMsgTxt.strOK;
                    resultBo.Insert();

                    //更新 TBLOG_VIDEO_HD_MC、通知轉低解
                    resultBo.vo.FCFILE_STATUS = dgv.CurrentRow.Cells["FCFILE_STATUS"].Value.ToString().Trim();
                    resultBo.vo.FSFILE_PATH = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                    resultBo.vo.FSFILE_PATH_L = dgv.CurrentRow.Cells["FSFILE_PATH_L"].Value.ToString().Trim();
                    resultBo.vo.FSARC_TYPE = dgv.CurrentRow.Cells["FSARC_TYPE"].Value.ToString().Trim();
                    resultBo.vo.FCSTATUS = GlobalMsgTxt.OK;
                    resultBo.Update();
                }
                else 
                {
                    MessageBox.Show("此素材已經審核確認過...");
                }

                GetDataReload();
            }
        }

        /// <summary>
        /// QC 檢查結果：不通過
        /// </summary>
        private void menuItemQCResultNG_Click(object sender, EventArgs e)
        {
            DataGridView dgv = dgvPlayList;

            if (dgv.CurrentRow.Selected)
            {
                if (!resultBo.GetQCFileIsChecked(dgv.CurrentRow.Cells["FSVIDEO_ID"].Value.ToString().Trim()))
                {
                    //新增 TBLOG_QC_RESULT
                    resultBo.vo.FSVIDEO_ID = dgv.CurrentRow.Cells["FSVIDEO_ID"].Value.ToString().Trim();
                    resultBo.vo.FSFILE_NO = dgv.CurrentRow.Cells["FSFILE_NO"].Value.ToString().Trim();
                    resultBo.vo.FSPROG_ID = dgv.CurrentRow.Cells["FSPROG_ID"].Value.ToString().Trim();
                    resultBo.vo.FSPROG_NAME = dgv.CurrentRow.Cells["FSPROG_NAME"].Value.ToString().Trim();
                    resultBo.vo.FNEPISODE = int.Parse(dgv.CurrentRow.Cells["FNEPISODE"].Value.ToString().Trim());
                    //resultBo.vo.FSBEG_TIMECODE = dgv.CurrentRow.Cells["FSBEG_TIMECODE"].Value.ToString().Trim();
                    //resultBo.vo.FSEND_TIMECODE = dgv.CurrentRow.Cells["FSEND_TIMECODE"].Value.ToString().Trim();
                    resultBo.vo.FSQC_DESCRIPTION = GlobalMsgTxt.strNG;
                    resultBo.Insert();

                    //更新 TBLOG_VIDEO_HD_MC、TBLOG_VIDEO(F)
                    resultBo.vo.FSFILE_NO = dgv.CurrentRow.Cells["FSFILE_NO"].Value.ToString().Trim();
                    resultBo.vo.FSPROG_ID = dgv.CurrentRow.Cells["FSPROG_ID"].Value.ToString().Trim();
                    resultBo.vo.FSARC_TYPE = dgv.CurrentRow.Cells["FSARC_TYPE"].Value.ToString().Trim();
                    resultBo.vo.FCSTATUS = GlobalMsgTxt.NG;
                    resultBo.Update();
                }
                else
                {
                    MessageBox.Show("此素材已經審核確認過...");
                }

                GetDataReload();
            }
        }

        /// <summary>
        /// QC 檢查備註及歷史紀錄
        /// </summary>
        private void menuItemQCResultNote_Click(object sender, EventArgs e)
        {
            DataGridView dgv = dgvPlayList;

            if (dgv.CurrentRow.Selected)
            {
                RadQCRecord vForm = new RadQCRecord();

                vForm.FSCHANNEL_ID = radDrpFSCHANNEL_ID.SelectedValue.ToString();
                vForm.FDDATE = radDtpFDDATE.Text;

                vForm.FSFILE_USEQC = bool.Parse(dgv.CurrentRow.Cells["FSFILE_USEQC"].Value.ToString());
                vForm.FSVIDEO_ID = dgv.CurrentRow.Cells["FSVIDEO_ID"].Value.ToString().Trim();
                vForm.FSFILE_NO = dgv.CurrentRow.Cells["FSFILE_NO"].Value.ToString().Trim();
                vForm.FSPROG_ID = dgv.CurrentRow.Cells["FSPROG_ID"].Value.ToString().Trim();
                vForm.FSPROG_NAME = dgv.CurrentRow.Cells["FSPROG_NAME"].Value.ToString().Trim();
                vForm.FNEPISODE = int.Parse(dgv.CurrentRow.Cells["FNEPISODE"].Value.ToString().Trim());
                vForm.FSFILE_PATH = dgv.CurrentRow.Cells["FSFILE_PATH"].Value.ToString().Trim();
                vForm.FSFILE_PATH_L = dgv.CurrentRow.Cells["FSFILE_PATH_L"].Value.ToString().Trim();
                vForm.FCFILE_STATUS = dgv.CurrentRow.Cells["FCFILE_STATUS"].Value.ToString().Trim();

                vForm.ShowDialog(this);

                GetDataReload();
            }
        }

        /// <summary>
        /// 複製影片編號
        /// </summary>
        private void menuItemCopyVideoID_Click(object sender, EventArgs e)
        {
            if (dgvPlayList.SelectedRows.Count > 0)
            {
                foreach (DataGridViewRow dgvRows in dgvPlayList.SelectedRows)
                {
                    Clipboard.SetText(dgvRows.Cells["FSVIDEO_ID"].Value.ToString().Trim(), TextDataFormat.Text);
                }

                GlobalMsgBox.ConfirmOK(String.Format("編號【{0}】已複製...", Clipboard.GetText(TextDataFormat.Text)), GlobalMsgTxt.InfoTitle);
            }
        }

        #endregion

        #endregion

        #region - Button -

        /// <summary>
        /// 查詢
        /// </summary>
        private void radBtnPlayListForQC_Click(object sender, EventArgs e)
        {
            if (QueryVerify())
            {
                GetData();
            }
            else 
            {
                MessageBox.Show(_ErrMsg);
            }
        }

        /// <summary>
        /// 重新整理
        /// </summary>
        private void radBtnPlayListReload_Click(object sender, EventArgs e)
        {
            if (QueryVerify())
            {
                GetDataReload();
                PlaylistRemove();
            }
            else
            {
                MessageBox.Show(_ErrMsg);
            }
        }

        /// <summary>
        /// 播表清單
        /// </summary>
        private void radBtnPlayList_Click(object sender, EventArgs e)
        {
            if (QueryVerify())
            {
                RadPlayList vForm = new RadPlayList();

                vForm.FSCHANNEL_ID = radDrpFSCHANNEL_ID.SelectedValue.ToString();
                vForm.FDDATE = radDtpFDDATE.Text;

                vForm.ShowDialog(this);

                GetDataReload();
            }
            else
            {
                MessageBox.Show(_ErrMsg);
            }
        }

        #endregion

        #endregion

        #region - 欄位和屬性 -

        /// <summary>錯誤訊息</summary>
        string _ErrMsg;

        /// <summary>列索引</summary>
        private int _FirstRowIndex = 0;

        /// <summary>列索引</summary>
        private int _RowIndex = 0;

        /// <summary>檔案路徑</summary>
        private string _FilePath;

        /// <summary>IN點</summary>
        private string _TCIn;

        /// <summary>OUT點</summary>
        private string _TCOut;

        /// <summary>定點</summary>
        private string _TCPos;

        private M_TIMECODE pTCIn;
        private M_TIMECODE pTCOut;
        private M_TIMECODE pTCPos;
        private MItem pFile;
        private string path;
        private double offset;

        #endregion
    }
}